<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Benno Fuenfstueck discovered that Pango, a library for layout and
rendering of text with an emphasis on internationalization, is prone to a
heap-based buffer overflow flaw in the pango_log2vis_get_embedding_levels
function. An attacker can take advantage of this flaw for denial of
service or potentially the execution of arbitrary code.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1.42.4-7~deb10u1.</p>

<p>We recommend that you upgrade your pango1.0 packages.</p>

<p>For the detailed security status of pango1.0 please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pango1.0">https://security-tracker.debian.org/tracker/pango1.0</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4496.data"
# $Id: $

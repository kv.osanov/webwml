<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>This update fixes multiple vulnerabilities in Imagemagick: Various memory
handling problems and cases of missing or incomplete input sanitising
may result in denial of service, memory disclosure or potentially the
execution of arbitrary code if malformed image files are processed.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 8:6.9.10.23+dfsg-2.1+deb10u1.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>For the detailed security status of imagemagick please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/imagemagick">\
https://security-tracker.debian.org/tracker/imagemagick</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4712.data"
# $Id: $

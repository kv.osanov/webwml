<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Miroslav Lichvar reported that the ptp4l program in linuxptp, an
implementation of the Precision Time Protocol (PTP), does not validate the
messageLength field of incoming messages, allowing a remote attacker to
cause a denial of service, information leak, or potentially remote code
execution.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1.8-1+deb9u1.</p>

<p>We recommend that you upgrade your linuxptp packages.</p>

<p>For the detailed security status of linuxptp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linuxptp">https://security-tracker.debian.org/tracker/linuxptp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2723.data"
# $Id: $

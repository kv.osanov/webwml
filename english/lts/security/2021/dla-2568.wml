<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a buffer overflow attack in the bind9 DNS
server caused by an issue in the GSSAPI ("Generic Security Services") security
policy negotiation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8625">CVE-2020-8625</a>

    <p>BIND servers are vulnerable if they are running an affected version and
    are configured to use GSS-TSIG features. In a configuration which uses
    BIND's default settings the vulnerable code path is not exposed, but a
    server can be rendered vulnerable by explicitly setting valid values for
    the tkey-gssapi-keytab or tkey-gssapi-credentialconfiguration options.
    Although the default configuration is not vulnerable, GSS-TSIG is
    frequently used in networks where BIND is integrated with Samba, as well as
    in mixed-server environments that combine BIND servers with Active
    Directory domain controllers. The most likely outcome of a successful
    exploitation of the vulnerability is a crash of the named process. However,
    remote code execution, while unproven, is theoretically possible. Affects:
    BIND 9.5.0 -&gt; 9.11.27, 9.12.0 -&gt; 9.16.11, and versions BIND 9.11.3-S1
    -&gt; 9.11.27-S1 and 9.16.8-S1 -&gt; 9.16.11-S1 of BIND Supported Preview
    Edition. Also release versions 9.17.0 -&gt; 9.17.1 of the BIND 9.17
    development branch</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:9.10.3.dfsg.P4-12.3+deb9u8.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2568.data"
# $Id: $

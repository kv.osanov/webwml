<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability was discovered in libxslt, a XSLT 1.0
processing library written in C.</p>

<p>In xsltCopyText in transform.c, a pointer variable is not reset under
certain circumstances. If the relevant memory area happened to be freed
and reused in a certain way, a bounds check could fail and memory
outside a buffer could be written to, or uninitialized data could be
disclosed.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.1.28-2+deb8u6.</p>

<p>We recommend that you upgrade your libxslt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1973.data"
# $Id: $

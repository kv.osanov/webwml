<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in XStream, a Java library to
serialize objects to XML and back again.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26258">CVE-2020-26258</a>

    <p>XStream is vulnerable to a Server-Side Forgery Request which can be
    activated when unmarshalling. The vulnerability may allow a remote attacker
    to request data from internal resources that are not publicly available
    only by manipulating the processed input stream.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26259">CVE-2020-26259</a>

    <p>Xstream is vulnerable to an Arbitrary File Deletion on the local host when
    unmarshalling. The vulnerability may allow a remote attacker to delete
    arbitrary known files on the host as long as the executing process has
    sufficient rights only by manipulating the processed input stream.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.4.11.1-1+deb9u1.</p>

<p>We recommend that you upgrade your libxstream-java packages.</p>

<p>For the detailed security status of libxstream-java please refer to
its security tracker page at:
<a  rel="nofollow" href="https://security-tracker.debian.org/tracker/libxstream-java">https://security-tracker.debian.org/tracker/libxstream-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a  rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2507.data"
# $Id: $

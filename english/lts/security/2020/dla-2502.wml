<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A potential denial-of-service attack through malicious timestamp tags
was fixed in PostSRSd, a Sender Rewriting Scheme (SRS) lookup table for
Postfix.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.4-1+deb9u1.</p>

<p>We recommend that you upgrade your postsrsd packages.</p>

<p>For the detailed security status of postsrsd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/postsrsd">https://security-tracker.debian.org/tracker/postsrsd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2502.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in libidn. The Common
Vulnerabilities and Exposures project identifies the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8948">CVE-2015-8948</a>

    <p>When idn is reading one zero byte as input an out-of-bounds-read occurred.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6261">CVE-2016-6261</a>

    <p>An out-of-bounds stack read is exploitable in idna_to_ascii_4i.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6263">CVE-2016-6263</a>

    <p>stringprep_utf8_nfkc_normalize reject invalid UTF-8, causes a crash.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.25-2+deb7u2.</p>

<p>We recommend that you upgrade your libidn packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-582.data"
# $Id: $

#use wml::debian::translation-check translation="938f7bfa52fa35f75b35de70bbd10bfc81e42e8d" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i bootloaderen GRUB2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14372">CVE-2020-14372</a>

    <p>Man opdagede at acpi-kommandoen tillod at en priviligeret bruger kunne 
    indlæse fabrikerede ACPI-tabeller når Secure Boot er aktiveret.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25632">CVE-2020-25632</a>

    <p>En sårbarhed i forbindelse med anvendelse efter frigivelse, blev fundet 
    i kommandoen rmmod.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25647">CVE-2020-25647</a>

    <p>En sårbarhed i forbindelse med skrivning udenfor grænserne, blev fundet i
    funktionen grub_usb_device_initialize(), som kaldes for at håndtere 
    initialisering af USB-enheder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27749">CVE-2020-27749</a>

    <p>En stakbufferoverløbsfejl blev fundet i 
    grub_parser_split_cmdline.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27779">CVE-2020-27779</a>

    <p>Man opdagede at kommandoen cutmem tillod at en priviligeret bruger kunne 
    fjerne hukommelsesområder når Secure Boot er aktiveret.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20225">CVE-2021-20225</a>

    <p>En sårbarhed i forbindelse med skrivning udenfor heapgrænserne, blev 
    fundet i fortolkeren af valgmuligheder på kort form.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20233">CVE-2021-20233</a>

    <p>En fejl i forbindelse med skrivning udenfor heapgrænserne, fandtes på 
    grund af en fejlberegning af krævet plads til citationstegn i dannelsen af 
    menuen.</p></li>

</ul>

<p>Yderligere detaljerede oplysninger finder man i 
<a href="https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot">\
https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot</a></p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 2.02+dfsg1-20+deb10u4.</p>

<p>Vi anbefaler at du opgraderer dine grub2-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende grub2, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/grub2">\
https://security-tracker.debian.org/tracker/grub2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4867.data"

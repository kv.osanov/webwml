# Jorge Barreiro <yortx.barry@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2012-12-09 21:19+0100\n"
"Last-Translator: Jorge Barreiro <yortx.barry@gmail.com>\n"
"Language-Team: Galician <proxecto@trasno.net>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Data"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Liña de tempo"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr ""

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Nomeamentos"

#: ../../english/template/debian/votebar.wml:25
#, fuzzy
msgid "Withdrawals"
msgstr "Retirada"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Debate"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Plataformas"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Propoñente"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Propoñente da proposta A"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Propoñente da proposta B"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Propoñente da proposta B"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Propoñente da proposta D"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Propoñente da proposta E"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Propoñente da proposta F"

#: ../../english/template/debian/votebar.wml:55
#, fuzzy
msgid "Proposal G Proposer"
msgstr "Propoñente da proposta A"

#: ../../english/template/debian/votebar.wml:58
#, fuzzy
msgid "Proposal H Proposer"
msgstr "Propoñente da proposta A"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Avalistas"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Avalan a proposta A"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Avalan a proposta B"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Avalan a proposta C"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Avalan a proposta D"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Avalan a proposta E"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Avalan a proposta F"

#: ../../english/template/debian/votebar.wml:82
#, fuzzy
msgid "Proposal G Seconds"
msgstr "Avalan a proposta A"

#: ../../english/template/debian/votebar.wml:85
#, fuzzy
msgid "Proposal H Seconds"
msgstr "Avalan a proposta A"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Oposición"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Texto"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Proposta A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Proposta B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Proposta C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Proposta D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Proposta E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Proposta F"

#: ../../english/template/debian/votebar.wml:112
#, fuzzy
msgid "Proposal G"
msgstr "Proposta A"

#: ../../english/template/debian/votebar.wml:115
#, fuzzy
msgid "Proposal H"
msgstr "Proposta A"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Opcións"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Propoñente de emendas"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Avalistas da emenda"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Texto da emenda"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Propoñente de emenda A"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Avalistas da emenda A"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Texto da emenda A"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Propoñente de emenda B"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Avalistas da emenda B"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Texto da emenda B"

#: ../../english/template/debian/votebar.wml:148
#, fuzzy
msgid "Amendment Proposer C"
msgstr "Propoñente de emenda A"

#: ../../english/template/debian/votebar.wml:151
#, fuzzy
msgid "Amendment Seconds C"
msgstr "Avalistas da emenda A"

#: ../../english/template/debian/votebar.wml:154
#, fuzzy
msgid "Amendment Text C"
msgstr "Texto da emenda A"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Emendas"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Procedementos"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Requirimento de maioría"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Datos e estatísticas"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Quórum"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Discusión mínima"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Votación"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Foro"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Resultado"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Agardando&nbsp;por&nbsp;patrocinadores"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "En&nbsp;discusión"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Votación&nbps;aberta"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Decidida"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Retirada"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Outra"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Páxina de inicio das votacións"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Receita"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Enviar unha proposta"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Emendar unha proposta"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Seguir unha proposta"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Ler un resultado"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Votar"

#use wml::debian::template title="Documentação obsoleta"
#use wml::debian::translation-check translation="4792632f7a20682a368627c66663b57ff1b3fa8b"

#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/obsolete.defs"

<h1 id="historical">Documentos históricos</h1>

<p>
Os documentos listados abaixo foram escritos há muito tempo e não estão
atualizados ou foram escritos para versões anteriores do Debian e não foram
atualizados para as versões atuais. Suas informações estão desatualizadas,
mas ainda podem ser do interesse de algumas pessoas.
</p>

<p>
Os documentos que perderam relevância e não servem mais para nada tiveram
suas referências removidas, mas o código-fonte de muitos desses manuais
obsoletos pode ser encontrado no
<a href="https://salsa.debian.org/ddp-team/attic">sótão do DDP</a>.
</p>


<h2 id="user">Documentação orientada ao(à) usuário(a)</h2>

<document "dselect Documentation for Beginners" "dselect">

<div class="centerblock">
<p>
Este arquivo documenta o dselect para usuários(as) iniciantes, e tem como
objetivo ajudar na instalação do Debian com sucesso. Ele não tenta explicar
tudo; portanto, quando você usar o dselect pela primeira vez, navegue nas
telas de ajuda.
</p>
<doctable>
  <authors "Stéphane Bortzmeyer">
  <maintainer "(?)">
  <status>
  parado: o <a href="https://packages.debian.org/aptitude">aptitude</a>
  substituiu o dselect como a interface padrão de gerenciamento de pacotes no
  Debian
  </status>
  <availability>
  <inddpvcs name="dselect-beginner" formats="html txt pdf ps"
            langs="ca cs da de en es fr hr it ja pl pt ru sk" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "User's Guide" "users-guide">

<div class="centerblock">
<p>
Este <q>guia do(a) usuário(a)</q> nada mais é que um <q>guia do(a) usuário(a)
do Progeny</q> reformatado.
Os conteúdos estão ajustados para o sistema Debian padrão.
</p>

<p>
Mais de 300 páginas com um bom tutorial para começar a usar o sistema Debian a
partir da
<acronym lang="en" title="Graphical User Interface">interface gráfica do(a) usuário(a)</acronym>
no desktop e da linha de comando shell.
</p>
<doctable>
  <authors "Progeny Linux Systems, Inc.">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  útil como um tutorial. Escrito para a versão woody, tornou-se obsoleto.
  </status>
  <availability>
# langs="en" isn't redundant, it adds the needed language suffix to the link
  <inddpvcs name="users-guide" index="users-guide" langs="en" formats="html txt pdf" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian Tutorial" "tutorial">

<div class="centerblock">
<p>
Este manual é para um(a) usuário(a) novato(a) no Linux, para ajudar esse(a)
usuário(a) a se familiarizar com o Linux após a instalação, ou para um(a)
usuário(a) novato(a) no Linux em um sistema que outra pessoa esteja administrando.
</p>
<doctable>
  <authors "Havoc Pennington, Oliver Elphick, Ole Tetlie, James Treacy,
  Craig Sawyer, Ivan E. Moore II">
  <editors "Havoc Pennington">
  <maintainer "(?)">
  <status>
  parado; incompleto;
  provavelmente obsoleto pela <a href="user-manuals#quick-reference">referência Debian</a>
  </status>
  <availability>
  ainda não está completo
  <inddpvcs name="debian-tutorial" vcsname="tutorial" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian GNU/Linux: Guide to Installation and Usage" "guide">

<div class="centerblock">
<p>
Um manual orientado para o(a) usuário(a) final.
</p>
<doctable>
  <authors "John Goerzen, Ossama Othman">
  <editors "John Goerzen">
  <status>
  pronto (mas é para o potato)
  </status>
  <availability>
  <inoldpackage "debian-guide">
  </availability>
</doctable>
</div>

<hr />

<document "Debian User Reference Manual" "userref">

<div class="centerblock">
<p>
Este manual fornece pelo menos uma visão geral de tudo o que um(a) usuário(a)
deve saber sobre seu sistema Debian GNU/Linux (ou seja, configurar o X, como
configurar a rede, acessar disquetes, etc). Ele pretende preencher a lacuna
entre o tutorial do Debian e o manual detalhado e as páginas de informações
fornecidas por cada pacote.
</p>

<p>
Ele também pretende dar uma alguma ideia de como combinar comandos, segundo o
princípio geral do Unix, de que <em>sempre existe mais de uma maneira de
fazer algo</em>.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Jason D. Waterman, Havoc Pennington,
      Oliver Elphick, Bruce Evry, Karl-Heinz Zimmer">
  <editors "Thalia L. Hooker, Oliver Elphick">
  <maintainer "(?)">
  <status>
  parado; bastante incompleto;
  provavelmente obsoleto pela <a href="user-manuals#quick-reference">referência Debian</a>
  </status>
  <availability>
  <inddpvcs name="user" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />


<document "Debian System Administrator's Manual" "system">

<div class="centerblock">
<p>
Este documento é mencionado na introdução do manual de políticas.
Ele cobre todos os aspectos de administração do sistema de um sistema Debian.
</p>
<doctable>
  <authors "Tapio Lehtonen">
  <maintainer "(?)">
  <status>
  parado; incompleto;
  provavelmente obsoleto pela <a href="user-manuals#quick-reference">referência Debian</a>
  </status>
  <availability>
  não disponível ainda
  <inddpvcs name="system-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian Network Administrator's Manual" "network">

<div class="centerblock">
<p>
Este manual cobre todos os aspectos de administração de rede de um sistema Debian.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Duncan C. Thomson, Ivan E. Moore II">
  <maintainer "(?)">
  <status>
  parado; incompleto;
  provavelmente obsoleto pela <a href="user-manuals#quick-reference">referência Debian</a>
  </status>
  <availability>
  não disponível ainda
  <inddpvcs name="network-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

# Adicione isso aos livros, há uma edição revisada (2º) @ Amazon
<document "The Linux Cookbook" "linuxcookbook">

<div class="centerblock">
<p>
Um guia prático de referência para o sistema Debian GNU/Linux que mostra, em
mais de 1.500 <q>receitas</q>, como usá-lo para atividades diárias &mdash; desde
o trabalho com texto, imagens e som até produtividade e problemas de rede.
Como o software que o livro descreve, o livro é livre (copyleft) e o fonte dos
dados está disponível.
</p>
<doctable>
  <authors "Michael Stutz">
  <status>
  publicado; escrito para a versão woody, tornou-se obsoleto.
  </status>
  <availability>
  <inoldpackage "linuxcookbook">
  <p><a href="http://dsl.org/cookbook/">do autor</a>
  </availability>
</doctable>
</div>

<hr />

<document "APT HOWTO" "apt-howto">

<div class="centerblock">
<p>
Este manual tenta ser uma fonte rápida, mas completa, de informações sobre o
sistema APT e seus recursos. Ele contém muitas informações sobre os principais
usos do APT e muitos exemplos.
</p>
<doctable>
  <authors "Gustavo Noronha Silva">
  <maintainer "Gustavo Noronha Silva">
  <status>
   obsoleto desde 2009
  </status>
  <availability>
  <inoldpackage "apt-howto">
  <inddpvcs name="apt-howto" langs="en ca cs de es el fr it ja pl pt-br ru uk tr zh-tw zh-cn"
	    formats="html txt pdf ps" naming="locale" vcstype="attic"/>
  </availability>
</doctable>
</div>


<h2 id="devel">Documentação para desenvolvedor(a)</h2>

<document "Introduction: Making a Debian Package" "makeadeb">

<div class="centerblock">
<p>
Introdução sobre como criar um <code>.deb</code>, usando
<strong>debmake</strong>.
</p>
<doctable>
  <authors "Jaldhar H. Vyas">
  <status>
  parado, obsoleto pelo <a href="devel-manuals#maint-guide">guia dos(as)
  novos(as) mantenedores(as)</a>
  </status>
  <availability>
  <a href="https://people.debian.org/~jaldhar/">HTML online</a>
  </availability>
</doctable>
</div>

<hr />

<document "Debian Programmers' Manual" "programmers">

<div class="centerblock">
<p>
Ajuda novos(as) desenvolvedores(as) a criar um pacote para o sistema Debian
GNU/Linux.
</p>
<doctable>
  <authors "Igor Grobman">
  <status>
  obsoleto pelo <a href="devel-manuals#maint-guide">guia dos(as) novos(as)
  mantenedores(as)</a>
  </status>
  <availability>
  <inddpvcs name="programmer" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian Packaging Manual" "packman">

<div class="centerblock">
<p>
Este manual descreve os aspectos técnicos da criação de pacotes binários e de
código-fonte do Debian. Ele também documenta a interface entre o dselect e seus
scripts de método de acesso. Ele não lida com os requisitos da política do
projeto Debian e assume familiaridade com as funções do dpkg da perspectiva
do(a) administrador(a) do sistema.

<doctable>
  <authors "Ian Jackson, Klee Dienes, David A. Morris, Christian Schwarz">
  <status>
  as partes que na realidade eram diretrizes foram incorporadas na
  <a href="devel-manuals#policy">política do Debian</a>.
  </status>
  <availability>
  <inoldpackage "packaging-manual">
  </availability>
</doctable>
</div>

<hr />

<document "How Software Producers can distribute their products directly in .deb format" "swprod">

<div class="centerblock">
<p>
Este documento pretende ser um ponto de partida para explicar como os(as)
produtores(as) de software podem integrar seus produtos ao Debian, quais
situações diferentes podem surgir, dependendo da licença dos produtos e das
escolhas dos(as) produtores(as), e quais são as possibilidades que existem. Ele
não explica como criar pacotes, mas referencia documentos que fazem exatamente
isso.

<p>
Você deve ler isso se não estiver familiarizado com o panorama geral da criação
e distribuição de pacotes Debian, e opcionalmente com a adição deles à
distribuição Debian.

<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  obsoleto
  </status>
  <availability>
  <inddpvcs-distribute-deb>
  </availability>
</doctable>
</div>

<hr />

<document "Introduction to i18n" "i18n">

<div class="centerblock">
<p>
Este documento descreve a ideia básica e o howto de l10n (localização),
i18n (internacionalização) e m17n (multilinguização) para programadores(as) e
mantenedores(as) de pacotes.

<p>
O objetivo deste documento é tornar mais pacotes compatíveis com o i18n e
tornar o Debian uma distribuição mais internacionalizada.
Contribuições de todo o mundo serão bem-vindas, porque o autor original é um
falante de japonês e este documento seria em japonês se não houvesse
contribuições.

<doctable>
  <authors "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <maintainer "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <status>
  parado, obsoleto
  </status>
  <availability>
  ainda não está completo
  <inddpvcs-intro-i18n>
  </availability>
</doctable>
</div>

<hr />

<document "Debian SGML/XML HOWTO" "sgml-howto">

<div class="centerblock">
<p>
Este HOWTO contém informações práticas sobre o uso de SGML e XML em um sistema
operacional Debian.

<doctable>
  <authors "Stephane Bortzmeyer">
  <maintainer "Stephane Bortzmeyer">
  <status>
  parado, obsoleto
  </status>
  <availability>

# Apenas inglês apenas usando index.html, então defina o langs.
  <inddpvcs name="sgml-howto"
            formats="html"
            srctype="SGML"
            vcstype="attic"
/>
  </availability>
</doctable>
</div>

<hr />

<document "Debian XML/SGML Policy" "xml-sgml-policy">

<div class="centerblock">
<p>
Subpolítica para pacotes Debian que fornecem e/ou fazem uso de recursos XML ou
SGML.

<doctable>
  <authors "Mark Johnson, Ardo van Rangelrooij, Adam Di Carlo">
  <status>
  morto
  </status>
  <availability>
  <inddpvcs-xml-sgml-policy>
  </availability>
</doctable>
</div>

<hr />

<document "DebianDoc-SGML Markup Manual" "markup">

<div class="centerblock">
<p>
Documentação para o sistema <strong>debiandoc-sgml</strong>, incluindo
melhores práticas e dicas para mantenedores(as). Versões futuras devem incluir
dicas para facilitar a manutenção e construção de documentação nos pacotes
Debian, diretrizes para organizar a tradução de documentação e outras
informações úteis.
Veja também o <a href="https://bugs.debian.org/43718">bug #43718</a>.

<doctable>
  <authors "Ian Jackson, Ardo van Rangelrooij">
  <maintainer "Ardo van Rangelrooij">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "debiandoc-sgml-doc">
  <inddpvcs-debiandoc-sgml-doc>
  </availability>
</doctable>
</div>

<h2 id="misc">Documentação diversa</h2>

<document "Debian Repository HOWTO" "repo">

<div class="centerblock">
<p>
Este documento explica como os repositórios Debian funcionam, como criá-los e
como adicioná-los ao <tt>sources.list</tt> corretamente.
</p>
<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  pronto (?)
  </status>
  <availability>
  <inddpvcs name="repository-howto" index="repository-howto"
            formats="html" langs="en fr de uk ta" vcstype="attic">
  </availability>
</doctable>
</div>

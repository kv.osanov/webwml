#use wml::debian::template title="O mecanismo <q>proposed-updates</q>"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="09c8de4214e938e26eeaf5c5d5bbb5937213f414"

<p>Todas as mudanças da versão <q>estável (stable)</q>
(e <q>antiga (oldstable)</q>) lançada passam por um período de testes extensivos
antes de serem aceitas no repositório. Essas atualizações da versão estável
(e da antiga) são chamadas de <q>versão pontual</q>.</p>

<p>A preparação para as versões pontuais é feita através do mecanismo
<q>proposed-updates</q>. Pacotes atualizados são enviados para uma fila
separada chamada <code>p-u-new</code> (<code>o-p-u-new</code>) antes de serem
aceitos no <q>proposed-updates</q> (e no <q>oldstable-proposed-updates</q>).
</p>

<p>Para usar esses pacotes com APT, você pode adicionar as seguintes linhas
no seu arquivo <code>sources.list</code>:</p>

<pre>
  \# atualizações propostas para a próxima versão pontual
  deb http://ftp.us.debian.org/debian <current_release_name>-proposed-updates main contrib non-free
</pre>

<p>Note que <a href="$(HOME)/mirror/list">os espelhos /debian/</a> terão
esses pacotes, não é necessário utilizar um espelho particular; a escolha
acima, ftp.us.debian.org, é somente um exemplo.</p>

<p>Novos pacotes podem chegar no proposed-updates quando os(as)
desenvolvedores(as) Debian os enviarem para <q>proposed-updates</q>
(<q>oldstable-proposed-updates</q>) ou para <q>estável</q> (<q>antiga</q>).
O <a href="$(HOME)/doc/manuals/developers-reference/pkgs.html#upload-stable">\
processo de upload</a> é descrito na referência para desenvolvedor(a).
</p>

<p>Deve-se observar que os pacotes do
<a href="$(HOME)/security/">security.debian.org</a> são copiados automaticamente
para o diretório p-u-new (o-p-u-new). Ao mesmo tempo, os pacotes que são
enviados diretamente para proposed-updates (oldstable-proposed-updates) não são
monitorados pelo time de segurança do Debian.</p>

<p>A lista atualizada de pacotes que estão na fila p-u-new (o-p-u-new)
pode ser vista em <url "https://release.debian.org/proposed-updates/stable.html">
(<url "https://release.debian.org/proposed-updates/oldstable.html">).</p>

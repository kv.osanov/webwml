#use wml::debian::template title="Debian-Acessibilidade - Software"
#use wml::debian::translation-check translation="82f3d1721149ec88f5974028ed2c2d8d454b4071" maintainer="Thiago Pezzo (Tico)"
{#style#:<link rel="stylesheet" href="style.css" type="text/css" />:#style#}

<define-tag a11y-pkg endtag=required>
<preserve name tag url/>
<set-var %attributes>
<h3><if "<get-var url>"
        <a href="<get-var url>" name="<get-var tag>"><get-var name></a>
      <a href="https://packages.debian.org/<get-var tag>" name="<get-var tag>"><get-var name></a>></h3>
  %body
<restore name tag url/>
</define-tag>

<h2><a id="speech-synthesis" name="speech-synthesis">Síntese de fala e APIs relacionadas</a></h2>

<p>
  Uma lista minuciosa está disponível na
    <a href="https://blends.debian.org/accessibility/tasks/speechsynthesis">página da tarefa speechsynthesis</a>
</p>

<a11y-pkg name="EFlite" tag=eflite url="http://eflite.sourceforge.net/">
<p>
  Um servidor de fala para o <a href="#emacspeak">Emacspeak</a> e o
  <a href="#yasr">yasr</a> (ou outros leitores de tela) que os permite fazer
  interface com o <a href="#flite">Festival Lite</a>, um motor livre para
  conversão de texto em fala desenvolvido pelo CMU Speech Center como um
  derivado do <a href="#festival">Festival</a>.
</p>
<p>
  Devido a limitações herdadas de seu backend, o EFlite somente provê suporte
  para a língua inglesa no momento.
</p>
</a11y-pkg>
<a11y-pkg name="eSpeak" tag=espeak>
<p>
O eSpeak/eSpeak-NG é um software sintetizador de fala para o inglês e alguns
outros idiomas.
</p>
<p>
O eSpeak produz fala em inglês de boa qualidade. Ele usa um método de
síntese diferente de outros motores de texto para fala (TTS - text to speech)
de código aberto (sem síntese de fala por concatenação, portanto ele deixa
pouco vestígio), e soa bem diferente. Talvez não seja tão natural ou
<q>suave</q>, mas algumas pessoas acham a articulação mais clara e fácil de
ouvir por longos períodos.
</p>
<p>
Ele pode ser executado via programa de linha de comando para falar texto de um
arquivo ou da entrada padrão (stdin). Ele também funciona como um <q>Talker</q>
(sintetizador de voz) com o sistema de texto para fala do KDE (KTTS - KDE text
to speech) como uma alternativa ao <a href="#festival">Festival</a>, por
exemplo. Nesse sentido, ele pode falar texto que foi selecionado para a área de
transferência, ou diretamente do navegador Konqueror ou do editor Kate.
</p>
  <ul>
    <li>Inclui diferentes vozes, cujas características podem ser alteradas.</li>
    <li>Pode produzir saída de fala como um arquivo WAV.</li>
    <li>Pode traduzir texto para códigos de fonemas, de modo que possa ser
    adaptado como uma interface para outro motor de síntese de voz.</li>
    <li>Potencial para outros idiomas. Tentativas ainda rudimentares (e
    provavelmente engraçadas) para o alemão e o esperanto estão incluídas.</li>
    <li>Tamanho compacto. O programa e seus dados somam por volta de 350 kbytes.</li>
    <li>Escrito em C++.</li>
  </ul>
<p>
eSpeak também pode ser usado com o
<a href="#speech-dispatcher">Speech Dispatcher</a>.
</p>
</a11y-pkg>
<a11y-pkg name="Festival Lite" tag=flite>
<p>
  Um motor de síntese de fala pequeno e de rápida execução. É a última adição ao
  pacote de ferramentas livres de softwares de síntese, incluindo o Festival
  Speech Synthesis System da Universidade de Edinburgh e o projeto FestVox da
  Universidade Carnegie Mellon; além de ferramentas, scripts e documentação para
  construção de vozes sintéticas. Contudo, o flite propriamente dito não requer
  nenhum desses sistemas para rodar.
</p>
<p>
  Atualmente só suporta a língua inglesa.
</p>
</a11y-pkg>
<a11y-pkg name="Festival" tag="festival"
          url="http://www.cstr.ed.ac.uk/projects/festival/">
<p>
  Um sistema genérico e multi-idioma de síntese de fala desenvolvido
  no <a href="http://www.cstr.ed.ac.uk/">CSTR</a> [<i>C</i>entre for
  <i>S</i>peech <i>T</i>echnology <i>R</i>esearch] da
  <a href="http://www.ed.ac.uk/text.html">Universidade de Edinburgh</a>.
</p>
<p>
  O Festival oferece um sistema completo de texto para fala com várias APIs, mas
  também um ambiente para desenvolvimento e pesquisa de técnicas de síntese de
  fala. É escrito em C++ com um interpretador de comando baseado em Scheme para
  controle geral.
</p>
<p>
  Além de pesquisa com síntese de fala, o Festival é útil como um programa
  autônomo de síntese de fala. Ele é capaz de produzir falas claramente
  compreensíveis dos textos.
</p>
</a11y-pkg>
<a11y-pkg name="Speech Dispatcher" tag="speech-dispatcher"
          url="http://www.freebsoft.org/speechd">
<p>
  Fornece uma camada independente de dispositivo para síntese de fala.
  Ele suporta vários softwares e hardwares de síntese de fala como
  backends e provê uma camada genérica para síntese de fala e para reprodução
  de dados PCM através daquelas diferentes backends para os aplicativos.
</p>
<p>
  Muitos conceitos de alto nível como enfileiramento versus interrupção de fala,
  e configurações de usuário(a) específicas ao aplicativo, são implementados de
  uma maneira independente ao dispositivo; consequentemente liberando o(a)
  programador(a) do aplicativo de ter que reinventar a roda.
</p>
</a11y-pkg>

<h2><a name="i18nspeech">Síntese de fala internacionalizada</a></h2>
<p>
Todas as atuais soluções livres disponíveis para síntese de fala baseada em
software parecem compartilhar uma deficiência comum: elas são restritas à língua
inglesa, oferecendo somente suporte muito marginal aos outros idiomas; ou em
muitos casos, não oferecendo suporte algum.
Entre todos os softwares livres de síntese de fala para Linux, somente o
CMU Festival suporta mais de uma língua natural. O CMU Festival pode sintetizar
inglês, espanhol e galês. Alemão não é suportado. Francês não é suportado. Russo
não é suportado. Em um momento em que internacionalização e localização são
tendências no mundo do software e dos serviços web, é razoável demandar que
pessoas com deficiência visual interessadas no Linux tenham que aprender inglês
somente para entender as saídas geradas pelo computador e para conduzir todas as
suas ações em uma língua estrangeira?
</p>
<p>
Infelizmente, a síntese de fala não é o projeto preferido que a Jane Hacker (uma
conhecida inventora) faz na casa dela. Criar um software sintetizador de fala
inteligível envolve tarefas que consomem tempo.
Síntese de fala por concatenação requer a criação cuidadosa de um banco de dados
de fonemas contendo todas as possíveis combinações de som para um determinado
idioma.
Regras que determinem a transformação da representação de texto em fonemas
individuais também precisam ser desenvolvidas e detalhadamente ajustadas,
geralmente demandando a divisão de sequências de caracteres em grupos lógicos
como orações, frases e palavras. Tal análise lexicográfica requer um léxico
específico ao idioma que raramente é lançado sob uma licença livre.
</p>
<p>
Um dos mais promissores sistemas de síntese de fala é o Mbrola, com bancos de
dados de fonemas para diversos idiomas. A síntese propriamente dita é software
livre. Infelizmente, os bancos de dados de fonemas são apenas para uso não
militar e não comercial. Temos uma ausência de bancos de dados de fonemas
para serem usados no sistema operacional Debian.
</p>
<p>
Sem um software sintetizador de fala amplamente multi-idioma, o Linux não pode
ser aceito por provedores de tecnologia assistiva e pessoas com deficiências
visuais. O que podemos fazer para melhorar isso?
</p>
<p>
Existem basicamente duas abordagens possíveis:
</p>
<ol>
<li>Organizar um grupo de pessoas dispostas a ajudar nesta questão e tentar,
ativamente, melhorar a situação. Isto pode ser um pouco complicado, já que
é necessário um grande conhecimento específico sobre síntese de fala, e tal
conhecimento não é fácil de se obter por meio de uma abordagem autodidata.
Entretanto, isto não deve desencorajar você. Se você acha que pode motivar um
grupo de pessoas grande o suficiente para alcançar algumas melhorias, pode ser
que valha a pena fazê-lo.</li>
<li>Obter financiamento e contratar um instituto que já tenha experiência na
criação dos bancos de dados de fonemas necessários, regras léxica e de
transformação. Esta abordagem tem a vantagem de obter uma maior probabilidade
de gerar resultados com qualidade e deve alcançar algumas melhorias muito mais
cedo do que a primeira abordagem. Claro que a licença sob a qual todo o trabalho
produzido deve ser lançado deve ser acordado antecipadamente, e deve passar
pelos requisitos da DFSG. A solução ideal seria, com certeza, convencer alguma
universidade a perfazer tal projeto com seu próprio dinheiro e contribuir com
os resultados para a comunidade de software livre.</li>
</ol>
<p>
Por fim, mas não menos importante, parece que os produtos de síntese de fala
de maior sucesso comercial hoje não usam mais a síntese de fala por
concatenação, principalmente porque os bancos de dados de sons consomem muito
espaço de disco. Isto não é realmente desejado para pequenos produtos
embarcados, como por exemplo fala em telefones celulares. Softwares livres
recentemente lançados como o <a href="#espeak">eSpeak</a> parecem tentar esta
abordagem, o que pode ser muito vantajoso dar uma olhada.
</p>

<h2><a id="emacs" name="emacs">Extensões do Emacs para leitura de tela</a></h2>
<a11y-pkg name="Emacspeak" tag="emacspeak"
          url="http://emacspeak.sourceforge.net/">
<p>
  Um sistema de produção de fala que permitirá a alguém com deficiência visual
  trabalhar diretamente em um sistema UNIX. Uma vez que se inicie o Emacs com o
  Emacspeak carregado, recebe-se um retorno falado para tudo o que se faz. Seu
  benefício dependerá de quão bem você usa o Emacs. Não há nada que você não
  possa fazer dentro do Emacs :-). Este pacote inclui servidores de fala
  escritos em tcl para suporte ao sintetizadores de fala DECtalk Express e
  DECtalk MultiVoice. Para outros sintetizadores, procure por pacotes de
  servidores de fala separados, como o Emacspeak-ss ou
  <a href="#eflite">eflite</a>.
</p>
</a11y-pkg>
<a11y-pkg name="speechd-el" tag="speechd-el"
          url="http://www.freebsoft.org/speechd-el">
<p>
  Cliente Emacs para sintetizadores de fala, linhas braille e
  outras interfaces de saída alternativas. Ele fornece um ambiente de saída
  completo de fala e braille para Emacs. Ele destina-se primeiramente para
  usuários(as) com deficiência visual que precisam de comunicação não visual com
  o Emacs, mas pode ser usado por qualquer pessoa que necessite de fala
  sofisticada ou outro tipo de saída alternativa do Emacs.
</p>
</a11y-pkg>

<h2><a id="console" name="console">Leitores de tela para console (modo texto)</a></h2>

<p>
  Uma lista minuciosa está disponível na
    <a href="https://blends.debian.org/accessibility/tasks/console">página da tarefa dos leitores de tela para console</a>
</p>

<a11y-pkg name="BRLTTY" tag="brltty" url="https://brltty.app/">
<p>
  Um daemon que fornece acesso ao console do Linux para uma pessoa com
  deficiência visual que esteja usando uma linha braille.
  Ele controla o terminal braille e fornece uma funcionalidade completa de
  leitura de tela.
</p>
<p>
 Os dispositivos Braille suportados pelo BRLTTY estão listados na
   <a href="https://brltty.app/doc/KeyBindings/#braille-device-bindings">
     documentação de dispositivos BRLTTY</a>
</p>
<p>
  O BRLTTY também fornece uma infraestrutura baseada em cliente/servidor para
  aplicações que desejem utilizar uma linha braille. O processo daemon aguarda
  por conexões TCP/IP de entrada em uma certa porta. Uma biblioteca compartilhada
  de objetos para clientes é fornecida no pacote
  <a href="https://packages.debian.org/libbrlapi">libbrlapi</a>. Uma biblioteca
  estática, arquivos de cabeçalho e documentação são fornecidos no pacote
  <a href="https://packages.debian.org/libbrlapi-dev">libbrlapi-dev</a>. Esta
  funcionalidade é, portanto, usada pelo <a href="#gnome-orca">Orca</a> para
  fornecer suporte para tipos de linhas braille que ainda não são suportadas
  diretamente pelo Gnopernicus.
</p>
</a11y-pkg>
<a11y-pkg name="Yasr" tag="yasr" url="http://yasr.sourceforge.net/">
<p>
  Um console de leitor de tela de propósito geral para GNU/Linux e outros
  sistemas operacionais UNIX-like. O nome <q>yasr</q> é um acrônimo que
  pode significar tanto <q>Yet Another Screen Reader</q> (ainda outro leitor de
  tela) ou <q>Your All-purpose Screen Reader</q> (seu leitor de tela para todos
  os propósitos).
</p>
<p>
  Atualmente, o yasr tenta suportar os hardwares sintetizadores Speak-out,
  DEC-talk, BNS, Apollo e DoubleTalk. Ele também consegue se comunicar com os
  servidores de fala Emacspeak e desse modo pode ser usado com sintetizadores
  que ainda não são suportados, como o <a href="#flite">Festival Lite</a> (via
  <a href="#eflite">eflite</a>) ou FreeTTS.
</p>
<p>
  O Yasr funciona abrindo um pseudo-terminal e executando um interpretador de
  comandos, interceptando todas as entradas e saídas. Ele olha para todas as
  sequências de escape que estão sendo enviadas e mantém uma <q>janela</q>
  virtual contendo o que ele acredita que esteja na tela. Ele então não usa
  qualquer funcionalidade específica do Linux e pode ser portado para outros
  sistemas operacionais UNIX-like sem muitas preocupações.
</p>
</a11y-pkg>


<h2><a id="gui" name="gui">Interfaces gráficas do(a) usuário(a)</a></h2>
<p>
Somente recentemente a acessibilidade em interfaces gráficas de usuários(as)
em plataformas UNIX receberam aperfeiçoamentos significativos com os vários
esforços de desenvolvimento em torno do
<a href="http://www.gnome.org/">Desktop GNOME</a>, especialmente o
<a href="https://wiki.gnome.org/Accessibility">projeto de acessibilidade do GNOME</a>.
(GNOME Accessibility Project).
</p>


<h2><a id="gnome" name="gnome">Software de acessibilidade do GNOME</a></h2>

<p>
  Uma lista minuciosa está disponível na
    <a href="https://blends.debian.org/accessibility/tasks/gnome">página da tarefa Gnome accessibility</a>
</p>

<a11y-pkg name="Assistive Technology Service Provider Interface" tag="at-spi">
<p>
  Este pacote contém os componentes essenciais de acessibilidade do GNOME.
  Ele permite que fornecedores de tecnologia assistiva, como leitores de tela,
  requisitem informações relativas à acessibilidade para todas as aplicações
  que estejam executando no desktop, como também fornece mecanismos de ligação
  para suportar outros conjuntos de ferramentas que não o GTK.
</p>
<p>
  Conexões com a linguagem Python são fornecidas no pacote
  <a href="https://packages.debian.org/python-at-spi">python-at-spi</a>.
</p>
</a11y-pkg>
<a11y-pkg name="The ATK accessibility toolkit" tag="atk">
<p>
  O ATK é um conjunto de ferramentas que fornece interfaces de acessibilidade
  para aplicações ou para outros conjuntos de ferramentas. Ao implementar essas
  interfaces, esses outros conjuntos de ferramentas ou aplicações podem ser
  usados com ferramentas como leitores de tela, lentes de aumento e dispositivos
  alternativos de entrada.
</p>
<p>
  A parte de runtime do ATK, necessária para executar aplicações construídas
  com ele, está disponível no pacote
  <a href="https://packages.debian.org/libatk1.0-0">libatk1.0-0</a>. Arquivos de
  desenvolvimento para o ATK, necessários para compilação de programas ou
  conjuntos de ferramentas que o utilizam, são fornecidos pelo pacote
  <a href="https://packages.debian.org/libatk1.0-dev">libatk1.0-dev</a>.
  Conexões com a linguagem Ruby são fornecidos pelo pacote
  <a href="https://packages.debian.org/ruby-atk">ruby-atk</a>.
</p>
</a11y-pkg>
<a11y-pkg name="gnome-accessibility-themes" tag="gnome-accessibility-themes">
<p>
  O pacote gnome-accessibility-themes contém alguns temas de grande
  acessibilidade para o ambiente de área de trabalho do GNOME, projetados para
  pessoas com baixa visão.
</p>
<p>
  Um total de 7 temas são fornecidos, possibilitando combinações de contraste
  alto, baixo ou invertido, como também texto e ícones ampliados.
</p>
</a11y-pkg>
<a11y-pkg name="gnome-orca" tag="gnome-orca"
          url="http://live.gnome.org/Orca">
<p>
  O Orca é um leitor de tela flexível e extensível que fornece acesso à área de
  trabalho gráfica através de combinações de fala, braille e/ou ampliação,
  combinações que podem ser personalizadas pelo(a) usuário(a). Sob o
  desenvolvimento da Sun Microsystems, Inc., Accessibility Program Office desde
  2004, o Orca foi criado desde o início com o envolvimento de seus(as)
  usuários(as) finais, e assim continua até hoje.
</p>
<p>
  O Orca pode usar o <a href="#speech-dispatcher">Speech Dispatcher</a> para
  prover saída de fala para o(a) usuário(a). O <a href="#brltty">BRLTTY</a> é
  usado para suporte à linha braille (e para integração suave com leitores
  braille de console e interface gráfica).
</p>
</a11y-pkg>


<h2><a id="kde" name="kde">Software de acessibilidade do KDE</a></h2>

<p>
  Uma lista minuciosa está disponível na
    <a href="https://blends.debian.org/accessibility/tasks/kde">página da tarefa KDE accessibility</a>
</p>

<a11y-pkg name="kmag" tag="kmag">
<p>
  Amplia uma parte da tela do mesmo modo como você usaria uma lente para ampliar
  uma letra pequena do jornal ou uma fotografia. Esta aplicação é útil para uma
  variedade de pessoas: de pesquisadores(as) a artistas, de designers web até
  para pessoas com baixa visão.
</p>
</a11y-pkg>


<h2><a id="input" name="input">Métodos de entrada não padronizados</a></h2>

<p>
  Uma lista minuciosa está disponível na
    <a href="https://blends.debian.org/accessibility/tasks/input">página da tarefa de métodos de entrada</a>
</p>

<a11y-pkg name="Dasher" tag="dasher" url="http://www.inference.phy.cam.ac.uk/dasher/">
<p>
  O Dasher é uma interface de entrada de texto eficiente quanto à informação,
  orientada por gestos naturais e contínuos de apontar. O Dasher é um sistema
  de entrada de texto interessante sempre que um teclado de tamanho completo não
  pode ser usado - por exemplo,
</p>
  <ul>
   <li>num computador palmtop</li>
   <li>num computador vestível</li>
   <li>ao operar um computador com uma mão, por joystick, tela sensível ao
       toque, trackball ou mouse</li>
   <li>ao operar um computador sem mãos (ou seja, com um mouse de cabeça ou com
       um sensor ocular).</li>
  </ul>
<p>
  A versão com sensor ocular do Dasher permite que um(a) usuário(a) experiente
  escreva texto tão rápido quanto uma escrita à mão normal - 25 palavras por
  minuto; usando um mouse, usuários(as) experientes podem escrever 39 palavras
  por minuto.
</p>
<p>
  O Dasher usa um algorítimo de predição mais avançado que o sistema T9(c)
  usado em telefones celulares, fazendo-o sensível ao contexto do entorno.
</p>
</a11y-pkg>
<a11y-pkg name="Caribou" tag="caribou" url="https://wiki.gnome.org/Projects/Caribou">
<p>
  O Caribou é uma tecnologia de entrada assistiva direcionada para usuários(as)
  de controles e gestos. Ele fornece um teclado configurável em tela com modo
  de escaneamento.
</p>
</a11y-pkg>

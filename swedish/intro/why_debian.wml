#use wml::debian::template title="Anledningar att välja Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7141cf5f25b03b1aa3c02c1afe2a5ca8cab2f672"

<p>Det finns en mängd anledningar till varför användare väljer Debian som deras operativsystem.</p>

<h1>Huvudorsaker</h1>

# try to list reasons for end users first

<dl>
  <dt><strong>Debian är fri mjukvara.</strong></dt>
  <dd>
    Debian är skapat av fri mjukvara och mjukvara med öppen källkod och kommer
    alltid att vara 100% <a href="free">fritt</a>. Fritt för alla att använda,
    modifiera, och distribuera. Detta är vårt huvudsakliga löfte till
    <a href="../users">våra användare</a>. Det är även kostnadsfritt.
  </dd>
</dl>

<dl>
  <dt><strong>Debian är ett stabilt och säkert Linuxbaserat operativsystem.</strong></dt>
  <dd>
    Debian är ett operativsystem för en bredd av enheter inklusive
    laptops, stationära datorer och servrar. Användare gillar dessa stabilitet
    och pålitlighet sedan 1993. Vi tillhandahåller rimliga standardinställningar
    för alla paket. Debianutvecklare tillhandahåller säkerhetsuppdateringar för
    alla paket över deras livstid när det är möjligt.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har omfattande hårdvarustöd.</strong></dt>
  <dd>
    Det mesta av alla hårdvara stöds redan av Linuxkärnan. Proprietära
    drivrutiner för hårdvara finns tillgänglig när fri mjukvara inte finns
    tillräcklig.
  </dd>
</dl>

<dl>
  <dt><strong>Debian tillhandahåller smidiga uppgraderingar.</strong></dt>
  <dd>
    Debian är välkänt för dess enkla och smidiga uppgraderingar inom en
    utgåvecykel, men även till nästa stora utgåva.
  </dd>
</dl>

<dl>
  <dt><strong>Debian är kärnan och basen för många andra distributioner</strong></dt>
  <dd>
    Många av de mest populära Linuxdistributionerna som Ubuntu, Knoppix, PureOS,
    SteamOS eller Tails, väljer Debian som en bas för deras mjukvara.
    Debian tillhandahåller alla verktyg så att alla kan utöka mjukvarupaketen
    från Debianarkivet med deras egna paket för deras behov.
  </dd>
</dl>

<dl>
  <dt><strong>Debianprojektet är en gemenskap.</strong></dt>
  <dd>
    Debian är inte bara ett Linuxoperativsystem. Mjukvaran samproduceras av
    hundratals frivilliga från hela världen. Du kan vara en del av
    Debiangemenskapen även om du inte är en programmerare eller
    systemadministratör. Debian är gemenskaps- och konsensusdriven och har en
    <a href="../devel/constitution">demokratisk ledningsstruktur</a>.
    Eftersom alla Debianutvecklare har lika rättigheter kan det inte
    kontrolleras av ett enstaka företag. Vi har utvecklare i mer än 60 länder
    och stödjer översättningar av vår Debianinstallerare i mer än 
    80 språk.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har flera installeraralternativ.</strong></dt>
  <dd>
    Slutanvändare kommer att använda vår
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live CD</a>
    inklusive den lättanvända Calamares-installeraren som behöver väldigt lite
    indata eller tidigare kunskap. Mer erfarna användare kan använda vår
    fullt utrustade installerare medan experter kan finjustera installationen
    eller till och med använda ett automatiserat nätverksinstallationsverktyg.
  </dd>
</dl>

<br>

<h1>Företagsmiljö</h1>

<p>
  Om du behöver Debian i en professionell miljö kan du dra nytta av
  dessa ytterligare fördelar:
</p>

<dl>
  <dt><strong>Debian är pålitligt.</strong></dt>
  <dd>
    Debian bevisar sin pålitlighet varje dag i tusentals verkliga
    scenarier från en laptop med en användare till superkolliderare,
    aktiemarknader och fordonsindustrin. Det är även populärt inom
    akademiska världen, inom vetenskap och i den offentliga sektorn.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har många experter.</strong></dt>
  <dd>
    Våra paketansvariga tar inte bara hand om Debianpaketering och
    nya uppströmsversioner. Ofta är de experter i uppströmsmjukvaran
    och bidrar till uppströmsutveckling direkt. Ibland är de även
    en del av uppströmsmjukvaran.
  </dd>
</dl>

<dl>
  <dt><strong>Debian är säkert.</strong></dt>
  <dd>
    Debian har säkerhetsstöd för dess stabila utgåvor. Många andra
    distributioner och säkerhetsforskare är beroende av Debians
    säkerhetsspårare.
  </dd>
</dl>

<dl>
  <dt><strong>Långtidsstöd.</strong></dt>
  <dd>
    Det finns <a href="https://wiki.debian.org/LTS">Långtidsstöd</a>
    (Long Term Support - LTS) utan extra kostnad. Detta ger dig
    utökat stöd för den stabila utgåvan i 5 år och mer. Utöver detta
    finns det även initiativet för <a 
    href="https://wiki.debian.org/LTS/Extended">Utökad LTS</a> som
    förlänger stödet för en begränsad uppsättning paket i mer än 5 år.
  </dd>
</dl>

<dl>
  <dt><strong>Molnavbildningar.</strong></dt>
  <dd>
    Officiella molnavbildningar finns tillgängliga för alla större
    molnplattformar. Vi tillhandahåller även verktygen och konfigurationen så
    att du kan bygga din egen anpassade molnavbildning. Du kan även använda
    Debian i virtuella maskiner på skrivbordet eller i en behållare.
  </dd>
</dl>

<br>

<h1>Utvecklare</h1>
<p>Debian används ofta av alla typer av mjukvaru- och hårdvaruutvecklare.</p>

<dl>
  <dt><strong>Publikt tillgängligt felspårningssystem.</strong></dt>
  <dd>
    Vårt <a href="../Bugs">felspårningssystem</a> (Bug tracking system - BTS)
    är publikt tillgängligt för alla via en webbläsare. Vi gömmer inte våra
    mjukvarufel och du kan enkelt skicka nya felrapporter.
  </dd>
</dl>

<dl>
  <dt><strong>IoT och inbäddade enheter.</strong></dt>
  <dd>
    Vi ger stöd till en bredd av enheter så som Raspberry Pi, varianter
    av QNAP, mobila enheter, hemrouters och en mängd enkortsdatorer (SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Flera hårdvaruarkitekturer.</strong></dt>
  <dd>
    Stöd för en <a href="../ports">lång lista</a> på CPU-arkitekturer
    inklusive amd64, i386, flera versioner av ARM och MIPS, POWER7, POWER8,
    IBM System z, RISC-V. Debian finns även tillgängligt för äldre och
    specifika nichearkitekturer.
  </dd>
</dl>

<dl>
  <dt><strong>Enormt antal mjukvarupaket tillgängliga</strong></dt>
  <dd>
    Debian har det största antalet installerade paket tillgängliga
    (för närvarande <packages_in_stable>). Våra paket använder deb-formatet
    som är välkänt för sin kvalitet.
  </dd>
</dl>

<dl>
  <dt><strong>Olika valmöjligheter rörande utgåvor.</strong></dt>
  <dd>
    Förutom vår stabila utgåva, får du de nyaste versionerna av mjukvara
    genom att använda uttestningsutgåvan eller den instabila utgåvan.
  </dd>
</dl>

<dl>
  <dt><strong>Hög kvalitet med hjälp av utvecklarverktyg samt policy.</strong></dt>
  <dd>
    Flera utvecklarverktyg hjälper oss att hålla kvaliteten på en hög
    nivå och vår <a href="../doc/debian-policy/">policy</a> definierar
    de tekniska kraven som varje paket måste tillfredsställa för att
    inkluderas i distributionen. Vår pågående integration kör
    autopkgtest-mjukvaran, piuparts är vårt testningsverktyg för
    installation, uppgradering och borttagning och lintian är en
    omfattande paketkontrollerare för Debianpaket.
  </dd>
</dl>

<br>

<h1>Vad våra användare säger</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      För mig är det den perfekta balansen mellan användarvänlighet och
      stabilitet. Jag har använt ett antal olika distributioner under åren
      men Debian är den enda som bara fungerar.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Orubbligt stabil. Massor av paket. Utmärkt gemenskap.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Debian för mig är en symbol för stabilitet och användarvänlighet.
    </strong></q>
  </li>
</ul>

#use wml::debian::template title="Innehållsförhandling"
#use wml::debian::translation-check translation="c646e774c01abc2ee3d32c65d6920ea4f03159dc"

<h3>Hur vet servern vilken fil den ska skicka?</h3>
<p>
Du kommer att upptäcka att interna länkar inte slutar med .html.
Detta beror på att servern använder innehållsförhanding (eng.
<em lang="en">content negotiation</em>) för att bestämma
vilken version av dokumentet som ska skickas ut.
När det finns mer än ett val kommer servern att göra upp en lista över alla
filer som kan skickas, t.ex om sidan &rdquo;about&rdquo; frågas så är listan
över filer kanske &rdquo;about.en.html&rdquo; och &rdquo;about.de.html&rdquo;.
Standardvalet för Debianservrarna är att skicka det engelska dokumentet, men
det är konfigurerbart.
</p>

<p>
Om klienten har den korrekta variabeln satt, t.ex för att få tyska
dokument, så kommer &rdquo;about.de.html&rdquo; att skickas i exemplet ovan.
Det som är trevligt med detta är att om det önskade språket inte är
tillgängligt, så kan en annan språkversion istället att skickas (vilket
förhoppningsvis är bättre än ingenting).
Valet av vilket dokument som ska skickas är kan vara lite förvirrande, så
istället för att beskriva det här så kan du hitta information på
<a href="https://httpd.apache.org/docs/current/content-negotiation.html">https://httpd.apache.org/docs/current/content-negotiation.html</a>
om du är intresserad.
</p>

<p>
Eftersom många användare inte ens vet vad innehållsförhandling är
finns det länkar nederst på varje sida som pekar direkt på alla andra
tillgängliga språkversioner.
Dessa länkas skapas av ett perl-skript som anropas av wml när sidan
genereras.
</p>

<p>Det finns även ett alternativ att överstyra inställningarna för språk i
webbläsaren med hjälp cookies som prioriterar ett språk före de andra
webbläsarinställningarna.</p>

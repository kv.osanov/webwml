#use wml::debian::translation-check translation="e381a4552e96e017684cf783d08d728d9ad833af"
<define-tag pagetitle>Debian 10 actualizado: publicada la versión 10.10</define-tag>
<define-tag release_date>2021-06-19</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.10</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la décima actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction apt "Por omisión, acepta cambios de nombre de distribución en repositorios (por ejemplo, stable -&gt; oldstable)">
<correction awstats "Corrige problemas de acceso a ficheros remotos [CVE-2020-29600 CVE-2020-35176]">
<correction base-files "Actualiza /etc/debian_version para la versión 10.10">
<correction berusky2 "Corrige violación de acceso en el arranque">
<correction clamav "Nueva versión «estable» del proyecto original; corrige problema de denegación de servicio [CVE-2021-1405]">
<correction clevis "Corrige soporte para los TPM que solo soportan SHA256">
<correction connman "dnsproxy: comprueba la longitud de las áreas de memoria antes de ejecutar memcpy [CVE-2021-33833]">
<correction crmsh "Corrige problema de ejecución de código [CVE-2020-35459]">
<correction debian-installer "Usa la ABI del núcleo Linux 4.19.0-17">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction dnspython "XFR: no intenta comparar con un valor de <q>expiration</q> no existente">
<correction dput-ng "Corrige caída del componente de transferencia («uploader») sftp en caso de que el servidor devuelva el error EACCES; actualiza nombres en clave; hace que <q>dcut dm</q> funcione para los y las DD sin permisos de subida; corrige un TypeError en la gestión de excepciones de subidas http; no intenta construir correo electrónico de subida desde el nombre de máquina system en ficheros .dak-commands">
<correction eterm "Corrige problema de ejecución de código [CVE-2021-33477]">
<correction exactimage "Corrige compilación con C++11 y OpenEXR 2.5.x">
<correction fig2dev "Corrige desbordamiento de memoria [CVE-2021-3561]; varias correcciones de la salida; recompila el juego de pruebas durante la compilación y en autopkgtest">
<correction fluidsynth "Corrige problema de «uso tras liberar» [CVE-2021-28421]">
<correction freediameter "Corrige problema de denegación de servicio [CVE-2020-6098]">
<correction fwupd "Corrige la generación de la cadena de caracteres SBAT del vendedor; deja de utilizar dpkg-dev en fwupd.preinst; nueva versión «estable» del proyecto original">
<correction fwupd-amd64-signed "Sincronizado con fwupd">
<correction fwupd-arm64-signed "Sincronizado con fwupd">
<correction fwupd-armhf-signed "Sincronizado con fwupd">
<correction fwupd-i386-signed "Sincronizado con fwupd">
<correction fwupdate "Mejora el soporte de SBAT">
<correction fwupdate-amd64-signed "Sincronizado con fwupdate">
<correction fwupdate-arm64-signed "Sincronizado con fwupdate">
<correction fwupdate-armhf-signed "Sincronizado con fwupdate">
<correction fwupdate-i386-signed "Sincronizado con fwupdate">
<correction glib2.0 "Corrige varios problemas de desbordamiento de entero [CVE-2021-27218 CVE-2021-27219]; corrige un ataque de enlace simbólico que afecta a file-roller [CVE-2021-28153]">
<correction gnutls28 "Corrige problema de desreferencia de puntero nulo [CVE-2020-24659]; añade varias mejoras a la reasignación de memoria">
<correction golang-github-docker-docker-credential-helpers "Corrige problema de «doble liberación» [CVE-2019-1020014]">
<correction htmldoc "Corrige problemas de desbordamiento de memoria [CVE-2019-19630 CVE-2021-20308]">
<correction ipmitool "Corrige problemas de desbordamiento de memoria [CVE-2020-5208]">
<correction ircii "Corrige problema de denegación de servicio [CVE-2021-29376]">
<correction isc-dhcp "Corrige problema de desbordamiento de memoria [CVE-2021-25217]">
<correction isync "Rechaza nombres <q>raros</q> de buzones de correo devueltos por IMAP LIST/LSUB [CVE-2021-20247]; corrige la gestión de códigos de respuesta APPENDUID inesperados [CVE-2021-3578]">
<correction jackson-databind "Corrige problema de expansión de entidad externa [CVE-2020-25649] y varios problemas relacionados con la serialización [CVE-2020-24616 CVE-2020-24750 CVE-2020-35490 CVE-2020-35491 CVE-2020-35728 CVE-2020-36179 CVE-2020-36180 CVE-2020-36181 CVE-2020-36182 CVE-2020-36183 CVE-2020-36184 CVE-2020-36185 CVE-2020-36186 CVE-2020-36187 CVE-2020-36188 CVE-2020-36189 CVE-2021-20190]">
<correction klibc "malloc: da valor a errno al fallar; corrige varios problemas de desbordamiento [CVE-2021-31873 CVE-2021-31870 CVE-2021-31872]; cpio: corrige posible caída en sistemas de 64 bits [CVE-2021-31871]; {set,long}jmp [s390x]: guarda/restaura los registros de FPU correctos">
<correction libbusiness-us-usps-webtools-perl "Actualiza a la nueva API US-USPS">
<correction libgcrypt20 "Corrige cifrado ElGamal débil con claves no generadas por GnuPG/libgcrypt [CVE-2021-33560]">
<correction libgetdata "Corrige problema de «uso tras liberar» [CVE-2021-20204]">
<correction libmateweather "Adaptado al cambio de nombre de America/Godthab como America/Nuuk en tzdata">
<correction libxml2 "Corrige lectura fuera de límites en xmllint [CVE-2020-24977]; corrige problemas de «uso tras liberar» en xmllint [CVE-2021-3516 CVE-2021-3518]; valida UTF8 en xmlEncodeEntities [CVE-2021-3517]; propaga error en xmlParseElementChildrenContentDeclPriv; corrige ataque de expansión exponencial de entidad [CVE-2021-3541]">
<correction liferea "Corrige compatibilidad con webkit2gtk &gt;= 2.32">
<correction linux "Nueva versión «estable» del proyecto original; incrementa la ABI a la 17; [rt] Actualizado a la 4.19.193-rt81">
<correction linux-latest "Actualizado a la ABI 4.19.0-17">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 17; [rt] Actualizado a la 4.19.193-rt81">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 17; [rt] Actualizado a la 4.19.193-rt81">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 17; [rt] Actualizado a la 4.19.193-rt81">
<correction mariadb-10.3 "Nueva versión del proyecto original; correcciones de seguridad [CVE-2021-2154 CVE-2021-2166 CVE-2021-27928]; corrige soporte de Innotop; incluye caching_sha2_password.so">
<correction mqtt-client "Corrige problema de denegación de servicio [CVE-2019-0222]">
<correction mumble "Corrige problema de ejecución de código remoto [CVE-2021-27229]">
<correction mupdf "Corrige problema de «uso tras liberar» [CVE-2020-16600] y problema de «doble liberación» [CVE-2021-3407]">
<correction nmap "Actualiza la lista de prefijos MAC incluida">
<correction node-glob-parent "Corrige problema de denegación de servicio con expresiones regulares [CVE-2020-28469]">
<correction node-handlebars "Corrige problemas de ejecución de código [CVE-2019-20920 CVE-2021-23369]">
<correction node-hosted-git-info "Corrige problema de denegación de servicio con expresiones regulares [CVE-2021-23362]">
<correction node-redis "Corrige problema de denegación de servicio con expresiones regulares [CVE-2021-29469]">
<correction node-ws "Corrige problema de denegación de servicio relacionado con expresiones regulares [CVE-2021-32640]">
<correction nvidia-graphics-drivers "Corrige vulnerabilidad de control de acceso incorrecto [CVE-2021-1076]">
<correction nvidia-graphics-drivers-legacy-390xx "Corrige vulnerabilidad de control de acceso incorrecto [CVE-2021-1076]; corrige fallo de instalación en versiones candidatas a definitiva de Linux 5.11">
<correction opendmarc "Corrige problema de desbordamiento de memoria dinámica («heap») [CVE-2020-12460]">
<correction openvpn "Corrige problema de cambio ilegal de dirección de cliente (<q>illegal client float</q>) [CVE-2020-11810]; se asegura de que el estado de la clave sea autenticado antes de enviar una respuesta push [CVE-2020-15078]; aumenta la cola de pendientes de listen() a 32">
<correction php-horde-text-filter "Corrige problema de ejecución de scripts entre sitios («cross-site scripting») [CVE-2021-26929]">
<correction plinth "Utiliza sesión para verificar el paso de bienvenida en el primer arranque">
<correction ruby-websocket-extensions "Corrige problema de denegación de servicio [CVE-2020-7663]">
<correction rust-rustyline "Corrige compilación con rustc más reciente">
<correction rxvt-unicode "Inactiva la secuencia de escape ESC G Q [CVE-2021-33477]">
<correction sabnzbdplus "Corrige vulnerabilidad de ejecución de código [CVE-2020-13124]">
<correction scrollz "Corrige problema de denegación de servicio [CVE-2021-29376]">
<correction shim "Nueva versión del proyecto original; añade soporte de SBAT; corrige reubicación de binarios en i386; no llama a QueryVariableInfo() en máquinas EFI 1.10 (por ejemplo, Macs Intel antiguos); corrige tratamiento de ignore_db y de user_insecure_mode; añade scripts del responsable del paquete a los paquetes plantilla para gestionar la instalación y eliminación de fbXXX.efi y de mmXXX.efi cuando instalamos/eliminamos los paquetes shim-helpers-$arch-signed; sale limpiamente si se instala en un sistema no EFI; no falla si las llamadas a debconf devuelven errores">
<correction shim-helpers-amd64-signed "Sincronizado con shim">
<correction shim-helpers-arm64-signed "Sincronizado con shim">
<correction shim-helpers-i386-signed "Sincronizado con shim">
<correction shim-signed "Actualizado para el nuevo shim; corrección de varios fallos en la gestión de postinst y de postrm; proporciona binarios sin firmar para arm64 (consulte NEWS.Debian); sale limpiamente si se instala en un sistema no EFI; no falla si las llamadas a debconf devuelven errores; corrige enlaces a la documentación; compilado contra shim-unsigned 15.4-5~deb10u1; añade dependencia explícita de shim-signed con shim-signed-common">
<correction speedtest-cli "Gestiona el caso de que <q>ignoreids</q> esté vacío o contenga ids vacíos">
<correction tnef "Corrige problema de lectura de memoria fuera de límites [CVE-2019-18849]">
<correction uim "libuim-data: copia <q>Rompe</q> de uim-data, corrigiendo algunos escenarios de actualización">
<correction user-mode-linux "Recompilado contra el núcleo Linux 4.19.194-1">
<correction velocity "Corrige problema de potencial ejecución de código arbitrario [CVE-2020-13936]">
<correction wml "Corrige regresión en el tratamiento de Unicode">
<correction xfce4-weather-plugin "Cambia a la API met.no versión 2.0">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2021 4848 golang-1.11>
<dsa 2021 4865 docker.io>
<dsa 2021 4873 squid>
<dsa 2021 4874 firefox-esr>
<dsa 2021 4875 openssl>
<dsa 2021 4877 webkit2gtk>
<dsa 2021 4878 pygments>
<dsa 2021 4879 spamassassin>
<dsa 2021 4880 lxml>
<dsa 2021 4881 curl>
<dsa 2021 4882 openjpeg2>
<dsa 2021 4883 underscore>
<dsa 2021 4884 ldb>
<dsa 2021 4885 netty>
<dsa 2021 4886 chromium>
<dsa 2021 4887 lib3mf>
<dsa 2021 4888 xen>
<dsa 2021 4889 mediawiki>
<dsa 2021 4890 ruby-kramdown>
<dsa 2021 4891 tomcat9>
<dsa 2021 4892 python-bleach>
<dsa 2021 4893 xorg-server>
<dsa 2021 4894 php-pear>
<dsa 2021 4895 firefox-esr>
<dsa 2021 4896 wordpress>
<dsa 2021 4898 wpa>
<dsa 2021 4899 openjdk-11-jre-dcevm>
<dsa 2021 4899 openjdk-11>
<dsa 2021 4900 gst-plugins-good1.0>
<dsa 2021 4901 gst-libav1.0>
<dsa 2021 4902 gst-plugins-bad1.0>
<dsa 2021 4903 gst-plugins-base1.0>
<dsa 2021 4904 gst-plugins-ugly1.0>
<dsa 2021 4905 shibboleth-sp>
<dsa 2021 4907 composer>
<dsa 2021 4908 libhibernate3-java>
<dsa 2021 4909 bind9>
<dsa 2021 4910 libimage-exiftool-perl>
<dsa 2021 4912 exim4>
<dsa 2021 4913 hivex>
<dsa 2021 4914 graphviz>
<dsa 2021 4915 postgresql-11>
<dsa 2021 4916 prosody>
<dsa 2021 4918 ruby-rack-cors>
<dsa 2021 4919 lz4>
<dsa 2021 4920 libx11>
<dsa 2021 4921 nginx>
<dsa 2021 4922 hyperkitty>
<dsa 2021 4923 webkit2gtk>
<dsa 2021 4924 squid>
<dsa 2021 4925 firefox-esr>
<dsa 2021 4926 lasso>
<dsa 2021 4928 htmldoc>
<dsa 2021 4929 rails>
<dsa 2021 4930 libwebp>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction sogo-connector "Incompatible con versiones actuales de Thunderbird">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>



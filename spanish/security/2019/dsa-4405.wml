#use wml::debian::translation-check translation="da347ceee9cca800740ef75deed5e600ef8e2b1d"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto múltiples vulnerabilidades en openjpeg2, el
codec JPEG 2000 de código abierto, que podrían utilizarse para provocar denegación
de servicio o, posiblemente, ejecución de código remoto.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17480">CVE-2017-17480</a>

    <p>Un desbordamiento de escritura en la pila en los codecs jp3d y jpwl puede dar lugar
    a denegación de servicio o a ejecución de código remoto a través de un fichero jp3d
    o jpwl manipulado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5785">CVE-2018-5785</a>

    <p>Un desbordamiento de entero puede dar lugar a denegación de servicio por medio de un fichero bmp
    manipulado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6616">CVE-2018-6616</a>

    <p>Un exceso de iteraciones puede dar lugar a denegación de servicio vía un fichero
    bmp manipulado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14423">CVE-2018-14423</a>

    <p>Vulnerabilidades de división por cero pueden dar lugar a denegación de servicio a través de
    un fichero j2k manipulado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18088">CVE-2018-18088</a>

    <p>Una desreferencia de puntero nulo puede dar lugar a denegación de servicio por medio de un
    fichero bmp manipulado.</p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 2.1.2-1.1+deb9u3.</p>

<p>Le recomendamos que actualice los paquetes de openjpeg2.</p>

<p>Para información detallada sobre el estado de seguridad de openjpeg2, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/openjpeg2">\
https://security-tracker.debian.org/tracker/openjpeg2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4405.data"

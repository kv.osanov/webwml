#use wml::debian::translation-check translation="eefd942b119f0fa7eb3adff259fef48f485b989e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans libwebp.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25009">CVE-2018-25009</a>

<p>Une lecture hors limites a été découverte dans la fonction
WebPMuxCreateInternal. Le plus grand risque de cette vulnérabilité concerne la
confidentialité des données et la disponibilité du service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25010">CVE-2018-25010</a>

<p>Une lecture hors limites a été découverte dans la fonction ApplyFilter.
Le plus grand risque de cette vulnérabilité concerne la confidentialité des
données et la disponibilité du service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25011">CVE-2018-25011</a>

<p>Un dépassement de tampon de tas a été découvert dans PutLE16().
Le plus grand risque de cette vulnérabilité concerne la confidentialité des
données et la disponibilité du service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25012">CVE-2018-25012</a>

<p>Une lecture hors limites a été découverte dans la fonction
WebPMuxCreateInternal. Le plus grand risque de cette vulnérabilité concerne la
confidentialité des données et la disponibilité du service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25013">CVE-2018-25013</a>

<p>Une lecture hors limites a été découverte dans la fonction dans ShiftBytes.
Le plus grand risque de cette vulnérabilité concerne la confidentialité des
données et la disponibilité du service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25014">CVE-2018-25014</a>

<p>Une variable non initialisée est utilisée dans la fonction ReadSymbol.
Le plus grand risque de cette vulnérabilité concerne la confidentialité et
l’intégrité des données ainsi que la disponibilité du système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36328">CVE-2020-36328</a>

<p>Un dépassement de tampon de tas dans la fonction WebPDecodeRGBInto est
possible à cause d’une vérification non valable de la taille de tampon. Le plus
grand risque de cette vulnérabilité concerne la confidentialité et l’intégrité
des données ainsi que la disponibilité du système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36329">CVE-2020-36329</a>

<p>Une utilisation de mémoire après libération a été découverte à cause d’un
processus léger tué de manière précoce. Le plus grand risque de cette
vulnérabilité concerne la confidentialité et l’intégrité des données ainsi que
la disponibilité du système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36330">CVE-2020-36330</a>

<p>Une lecture hors limites a été découverte dans la fonction
ChunkVerifyAndAssign. Le plus grand risque de cette vulnérabilité concerne la
confidentialité des données et la disponibilité du service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36331">CVE-2020-36331</a>

<p>Une lecture hors limites a été découverte dans la fonction ChunkAssignData.
Le plus grand risque de cette vulnérabilité concerne la confidentialité des
données et la disponibilité du service.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.5.2-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libwebp.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libwebp, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libwebp">\
https://security-tracker.debian.org/tracker/libwebp</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2677.data"
# $Id: $

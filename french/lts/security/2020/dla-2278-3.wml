#use wml::debian::translation-check translation="2f3a369c3fa9a0a77c93190400ce080d5ec3235e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de squid3 publiée dans la DLA-2278-2 introduisait une
régression due au correctif mis à jour pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12529">CVE-2019-12529</a>.
Le nouveau code d’authentification de Kerberos empêche la négociation de jeton
base64. Des paquets squid3 actualisés sont maintenant disponibles pour corriger
ce problème.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 3.5.23-5+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets squid3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de squid3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/squid3">https://security-tracker.debian.org/tracker/squid3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2278-3.data"
# $Id: $

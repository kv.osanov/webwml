#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans QEMU, un émulateur
rapide de processeur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17958">CVE-2018-17958</a>

<p>L’émulateur rtl8139 est sujet à un dépassement d'entier résultant en un
dépassement de tampon. Cette vulnérabilité peut être déclenchée par des
attaquants distants à l’aide de paquets contrefaits pour réaliser un déni
de service (à l’aide de l’accès au tampon de pile OOB).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19364">CVE-2018-19364</a>

<p>Le sous-système 9pfs est sujet à une situation de compétition permettant
aux processus légers de modifier un chemin fid alors qu’un autre processus
y a accès, menant (par exemple) à une utilisation de mémoire après
libération. Cette vulnérabilité peut être déclenchée par des attaquants
locaux pour réaliser un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19489">CVE-2018-19489</a>

<p>Le sous-système 9pfs est sujet à une situation de compétition lors du
renommage de fichier. Cette vulnérabilité peut être déclenchée par des
attaquants locaux pour réaliser un déni de service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 1:2.1+dfsg-12+deb8u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1646.data"
# $Id: $

#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le correctif pour <a href="https://security-tracker.debian.org/tracker/CVE-2017-14990">CVE-2017-14990</a>
publié sous DLA-1151-1 était incomplet et causait une régression. Il a été
découvert qu’une mise à niveau et des modifications de code supplémentaires
seraient nécessaires. Actuellement, ces modifications sont considérées comme
trop intrusives et par conséquent, le correctif original pour
<a href="https://security-tracker.debian.org/tracker/CVE-2017-14990">CVE-2017-14990</a>
a été retiré. Pour référence, le texte de l’annonce originale suit.</p>

<p>WordPress stocke en texte pur des valeurs de clef wp_signups.activation_key
(mais stocke les valeurs analogues wp_users.user_activation_key sous
forme de hachages), ce qui facilite pour des attaquants distants le détournement
de comptes inactifs d’utilisateurs en exploitant l’accès en lecture à la base
de données (tel qu’un accès obtenu à travers une vulnérabilité non précisée
d’injection SQL).</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.6.1+dfsg-1~deb7u19.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1151-2.data"
# $Id: $

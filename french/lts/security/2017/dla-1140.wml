#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Immédiatement après la mise à jour précédente de graphicsmagick, deux
problèmes supplémentaires de sécurité ont été identifiés. Les mises à jour sont
incluses ici.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13737">CVE-2017-13737</a>

<p>Arrondi incorrect aboutissant dans le brouillage du tas au-delà de
l’allocation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15277">CVE-2017-15277</a>

<p>Conservation de la palette non initialisée lors du traitement d’un fichier
GIF n’ayant pas de palette globale ni de palette locale.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.3.16-1.1+deb7u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphicsmagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1140.data"
# $Id: $

#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8400">CVE-2017-8400</a>

<p>Dans SWFTools 0.9.2, une écriture hors limites de données de tas peut
apparaître dans la fonction png_load() dans lib/png.c:755. Ce problème peut être
déclenché par un fichier PNG mal formé et dont le traitement est mal géré par png2swf. Des attaquants
pourraient exploiter ce problème pour un déni de service. Il pourrait causer
l’exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8401">CVE-2017-8401</a>

<p>Dans SWFTools 0.9.2, une lecture hors limites de données de tas peut
apparaître dans la fonction lea png_load() dans lib/png.c:724. Ce problème peut
être déclenché par un fichier PNG mal formé et dont le traitement est mal géré par png2swf. Des attaquants
pourraient exploiter ce problème pour un déni de service.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.9.2+ds1-3+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets swftools.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-995.data"
# $Id: $

#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans OpenSSH, une implémentation
libre de la suite de protocole SSH.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5352">CVE-2015-5352</a>

<p>OpenSSH vérifiait incorrectement la date limite d’écrans pour des
connexions X. Des attaquants distants pourraient exploiter ce défaut pour
contourner les restrictions d’accès voulues. Signalé par Jann Horn.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5600">CVE-2015-5600</a>

<p>OpenSSH restreignait improprement le traitement de périphériques interactifs
avec le clavier lors d’une connexion unique. Cela pourrait permettre à des
attaquants distants de réaliser des attaques par force brute ou causer un déni
de service dans une configuration personnalisée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6563">CVE-2015-6563</a>

<p>OpenSSH gérait incorrectement les noms d’utilisateur lors de
l’authentification PAM. En conjonction avec un autre défaut dans le processus
fils sans droits d’OpenSSH, des attaquants distants pourraient utiliser ce
problème pour réaliser une usurpation d’utilisateur. Découvert par Moritz
Jodeit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6564">CVE-2015-6564</a>

<p>Moritz Jodeit a découvert un défaut d’utilisation de mémoire après libération
dans la prise en charge de PAM dans OpenSSH. Cela pourrait être utilisé par des
attaquants distants pour contourner l’authentification ou, éventuellement,
exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1908">CVE-2016-1908</a>

<p>OpenSSH gérait incorrectement les redirections X11 non authentifiées lorsqu’un
serveur X désactivait l’extension SECURITY. Des connexions non authentifiées
pourraient obtenir les droits de redirection X11 authentifiée. Signalé par
Thomas Hoger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3115">CVE-2016-3115</a>

<p>OpenSSH gérait improprement les données de redirection X11 relatives aux
identifiants. Des utilisateurs distants authentifiés pourraient utiliser ce
défaut pour contourner les restrictions de commandes d’interpréteur voulues.
Découvert par github.com/tintinweb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6515">CVE-2016-6515</a>

<p>OpenSSH ne limitait par la longueur des mots de passe lors de
l’authentification. Des attaquants distants pourraient utiliser ce défaut pour
provoquer un déni de service à l’aide de longues chaînes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10009">CVE-2016-10009</a>

<p>Jann Horn a découvert une vulnérabilité de chemin de recherche non fiable
dans ssh-agent permettant à des attaquants distants d’exécuter des modules
PKCS#11 locaux arbitraires en exploitant le contrôle sur une socket d’agent
redirigée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10011">CVE-2016-10011</a>

<p>Jann Horn a découvert que OpenSSH n’examinait pas correctement les effets de
réallocation de contenu de tampon. Cela pourrait permettre à des utilisateurs locaux
d’obtenir des informations de clef privée sensibles en exploitant l’accès à un
processus fils avec des droits distincts.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10012">CVE-2016-10012</a>

<p>Guido Vranken a découvert que le gestionnaire de mémoire partagée d’OpenSSH
ne garantissait pas que la vérification de limites était appliquée pour tous
les compilateurs. Cela pourrait permettre à des utilisateurs locaux d’obtenir
des droits en exploitant l’accès au processus de bac à sable de droits
distincts.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10708">CVE-2016-10708</a>

<p>Déréférencement de pointeur NULL et plantage du démon à l'aide d'un message
NEWKEYS pas dans le bon ordre.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15906">CVE-2017-15906</a>

<p>Michal Zalewski a signalé qu’OpenSSH empêchait improprement les opérations
d’écriture dans le mode écriture seulement. Cela permettait à des attaquants de
créer des fichiers de longueur zéro.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 1:6.7p1-5+deb8u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssh.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1500.data"
# $Id: $

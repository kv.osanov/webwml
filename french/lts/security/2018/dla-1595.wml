#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Gnuplot, un programme de tracé interactif en LDC, a été étudié avec des tests
à données aléatoires par Tim Blazytko, Cornelius Aschermann, Sergej Schumilo et
Nils Bars. Ils ont trouvé divers cas de dépassement qui pourraient aboutir
à l'exécution de code arbitraire.</p>

<p>À cause de la chaîne de compilation spéciale pour durcissement dans Debian,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-19492">CVE-2018-19492</a>
ne concerne pas la sécurité, mais est un bogue et le correctif à été appliqué
par souci d'exhaustivité. Probablement, quelques projets aval n’ont pas les
mêmes réglages de chaîne de compilation.</p>


<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 5.0.0~rc+dfsg2-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gnuplot5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1595.data"
# $Id: $

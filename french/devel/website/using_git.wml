#use wml::debian::template title="Utilisation de git pour gérer le code source du site web"
#use wml::debian::translation-check translation="74a4415fa07f5810685f1d0568bf4b695cc931b3" maintainer="Jean-Pierre Giraud"

# Translator:
# Jean-Pierre Giraud, 2018.

<h2>Introduction</h2>

<p>Git est un <a href="https://fr.wikipedia.org/wiki/Version_control">système
de gestion de versions</a> qui aide à la gestion de travaux simultanés sur des
documents communs par plusieurs personnes. Chaque utilisateur peut détenir une
copie locale du dépôt principal. Ces copies peuvent être sur la même machine ou
aux quatre coins du monde. Les utilisateurs peuvent alors modifier leur
copie locale comme ils le souhaitent et, quand la version modifiée est
prête, enregistrer leurs changements (« commit ») et les envoyer en retour
vers le dépôt principal.</p>

<p>Git ne vous permettra pas d'envoyer directement une modification si le
dépôt distant a reçu des modifications (commits) plus récentes que votre copie
locale, dans la même branche.
Dans le cas où un tel conflit survient, veuillez d'abord rechercher et
appliquer les mises à jour de votre copie locale et rebaser (<code>rebase</code>)
vos nouvelles modifications au-dessus du dernier « commit » envoyé.
</p>

<h3><a name="write-access">Accès en écriture au dépôt de Git</a></h3>

<p>
La totalité du code source du site web de Debian est géré avec Git. Vous
le trouverez à l'adresse <url https://salsa.debian.org/webmaster-team/webwml/>.
Par défaut, les utilisateurs ne sont pas autorisés à envoyer de « commits »
sur le dépôt du code source. Vous aurez besoin de certaines permissions
pour obtenir un accès en écriture au dépôt.
</p>

<h4><a name="write-access-unlimited">Accès en écriture illimité</a></h4>

<p>
Si vous avez besoin d'un accès en écriture illimité au dépôt (par exemple,
vous êtes sur le point de devenir un contributeur régulier), veuillez en
faire la demande à l'aide de l'interface web
<url https://salsa.debian.org/webmaster-team/webwml/> après vous être
connecté à la plateforme Salsa de Debian.
</p>

<p>
Si vous débutez dans le développement du site web de Debian et si vous
n'avez pas d'expérience antérieure, veuillez aussi envoyer un courriel à <a
href="mailto:debian-www@lists.debian.org">debian-www@lists.debian.org</a>
en vous présentant avant de demander l'accès en écriture illimité. Veuillez
indiquer quelque chose d'utile dans votre présentation, comme la langue ou
la partie du site web auxquelles vous voudriez contribuer, et qui pourrait
se porter garant pour vous.
</p>

<h4><a name="write-access-via-merge-request">Écrire dans le dépôt avec des « merge requests »</a></h4>
<p>
Si vous n'avez pas l'intention de demander un accès en écriture illimité
au dépôt ou si vous n'êtes pas à même de le faire, vous pouvez toujours
soumettre une demande de fusion (« merge request ») et laisser d'autres
développeurs revoir et accepter votre travail. Veuillez soumettre les
« merge requests » en utilisant la procédure standard fournie par la
plateforme GitLab de Salsa à travers son interface web (consultez <a
href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Project forking workflow</a>
et <a
href="https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork">When you work in a fork</a>
pour plus de détails.
</p>

<p>
Les demandes de fusion ne sont pas suivies par tous les développeurs du
site web, elles peuvent donc ne pas être toujours traitées à temps. Si vous
n'êtes pas sûr que votre contribution sera acceptée, envoyez un message à
la liste de diffusion <a
href="https://lists.debian.org/debian-www/">debian-www</a> et demandez une
relecture.
</p>

<h2><a name="work-on-repository">Travailler sur le dépôt</a></h2>

<h3><a name="get-local-repo-copy">Obtenir une copie locale du dépôt</a></h3>

<p>D'abord, vous devez installer git pour travailler avec le dépôt. Ensuite,
configurez vos informations d'utilisateur et d'adresse courriel sur
votre ordinateur (veuillez vous référer à la documentation générale de git pour
apprendre comment le faire). Ensuite, vous pouvez cloner le dépôt (en d'autres
termes, vous en faites une copie) en utilisant une de ces deux méthodes.</p>

<p>La façon recommandée de travailler sur webwml est d'abord d'ouvrir un compte
sur salsa.debian.org et d'autoriser un accès SSH à git en envoyant une
clé publique SSH dans votre compte salsa. Voir les <a
href="https://salsa.debian.org/help/ssh/README.md">pages d'aide de salsa</a>
pour obtenir plus de détails sur comment le faire. Vous pouvez ensuite cloner
le dépôt webwml avec la commande suivante :</p>

<pre>
   git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>

<p>Si vous n'avez pas de compte salsa, une méthode alternative est de cloner le
dépôt avec le protocole HTTPS :</p>

<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>

<p>Cela vous donnera la même copie locale du dépôt, mais vous ne serez pas
capable d'envoyer directement vos modifications de cette manière.</p>

<p>Cloner la totalité du dépôt webwml demandera le téléchargement d'environ
500 Mo de données, cela peut donc être difficile pour tous ceux qui ont une
liaison Internet lente ou instable. Vous pouvez essayer de procéder à un
clonage superficiel avec d'abord une profondeur minimale pour un téléchargement
initial plus court :</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>Une fois que vous avez obtenu un dépôt (superficiel) utilisable, vous pouvez
approfondir la copie locale superficielle et, finalement, la convertir en un
dépôt local complet :</p>

<pre>
  git fetch --deepen=1000 # approfondit le dépôt de 1000 commits supplémentaires
  git fetch --unshallow   # obtient tous les commits manquants, convertit le dépôt en dépôt complet
</pre>
<h4><a name="partial-content-checkout">Récupération partielle de contenu</a></h4>

<p>Vous pouvez procéder à la récupération de seulement un sous-ensemble des
pages ainsi :</p>

<pre>
   $ git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git
   $ cd webwml
   $ git config core.sparseCheckout true
</pre>
   Dans webwml : créez le fichier .git/info/sparse-checkout avec un contenu
   comme celui-ci (si vous ne voulez que les fichiers de base, l'anglais et
   les traductions en français et en espagnol) :
<pre>
      /*
      !/[a-z]*/
      /english/
      /french/
      /spanish/
</pre>
   puis :
<pre>
   $ git checkout --
</pre>

<h3><a name="submit-changes">Soumettre des modifications locales</a></h3>

<h4><a name="keep-local-repo-up-to-date">Garder à jour le dépôt local</a></h4>

<p>De temps en temps (et au moins avant de débuter un travail d'édition !) il
faudra lancer la commande</p>

<pre>
   git pull
</pre>

<p>pour récupérer tous les fichiers du dépôt qui ont changé.</p>

<p>
Il est fortement recommandé de garder propre son répertoire local de travail de
git avant d'exécuter la commande « git pull » et de poursuivre le travail
d'édition. Si vous avez des modifications qui n'ont pas fait l'objet de
commits ou des commits locaux qui ne sont pas dans le dépôt distant dans la
branche courante, exécuter « git pull » créera immédiatement des demandes de
fusion ou même échouera dû à des conflits. Vous devriez envisager de garder
votre travail inachevé dans une autre branche ou d'utiliser des commandes
telles que « git stash ».
</p>

<p>Attention : git est un système de contrôle de versions distribué (non
centralisé). Cela signifie que quand vous préparez l'envoi de vos modifications,
elles seront seulement stockées dans votre dépôt local. Pour les partager avec
d'autres, vous devrez aussi envoyer vos modifications vers le dépôt central de
Salsa.</p>

<h4><a name="example-edit-english-file">Exemple de modifications de fichiers anglais</a></h4>

<p>
Un exemple de la manière dont des fichiers anglais peuvent être modifiés
dans le dépôt source du site web est fourni ici. Après avoir obtenu une
copie locale du dépôt avec la commande « git clone » et avant de commencer
le travail de modification, exécutez la commande suivante :
</p>

<pre>
   $ git pull
</pre>

<p>Maintenant, modifiez les fichiers. Quand c'est fait, préparez l'envoi de vos
modifications dans votre dépôt local avec les commandes :</p>

<pre>
   $ git add chemin/vers/fichier(s)
   $ git commit -m "Votre message d'envoi"
</pre>

<p>Si vous avez un accès en écriture illimité au dépôt webwml distant, vous
pouvez maintenant envoyer vos modifications dans le dépôt de Salsa :</p>

<pre>
   $ git push
</pre>

<p>Si vous n'avez pas un accès en écriture direct au dépôt webwml, vous
devez envisager de soumettre vos modifications en utilisant la fonction « Merge
Request » telle qu'elle est fournie par la plateforme GitLab de Salsa ou en
sollicitant l'aide d'autres développeurs.
</p>

<p>Il s'agit d'un résumé très sommaire de la façon d'utiliser git pour manipuler
le code source du site web de Debian. Pour plus d'informations sur git, veuillez
consulter sa documentation.</p>

<h4><a name="closing-debian-bug-in-git-commits">Fermer des bogues de Debian dans les envois de git</a></h4>

<p>
En incluant <code>Closes: #</code><var>nnnnnn</var> dans votre entrée de
journal d'envoi, le bogue <code>nº</code><var>nnnnnn</var> sera
fermé automatiquement quand vous enverrez vos modifications. La forme exacte
est la même que celle décrite dans
<a href="$(DOC)/debian-policy/ch-source.html#id24">la Charte Debian</a>.</p>

<h4><a name="links-using-http-https">Connexion en utilisant HTTP/HTTPS</a></h4>

<p>
Beaucoup de sites web de Debian prennent en charge SSL/TLS, veuillez donc
utiliser des liens en HTTPS lorsque c'est possible et pertinent.
<strong>Néanmoins</strong>, certains sites web de Debian, DebConf, SPI, etc.,
ne prennent pas en charge HTTPS ou n'ont leur certificat SSL signé que par SPI,
(et pas par une autorité SSL considérée de confiance par tous les navigateurs).
Veuillez éviter les liens en HTTPS vers ces sites web pour que les visiteurs ne
reçoivent pas de messages d'erreur.</p>

<p>Le dépôt git rejettera les envois contenant des liens en HTTP pur vers les
sites web Debian qui prennent en charge HTTPS, ou contenant des liens en HTTPS
vers les sites web Debian, DebConf et SPI qui sont connus pour ne pas prendre en
charge HTTPS ou avoir un certificat signé uniquement par SPI.</p>

<h3><a name="translation-work">Travailler sur des traductions</a></h3>

<p>
Les traductions doivent être toujours tenues à jour avec les fichiers anglais
correspondants. L'en-tête <q>translation-check</q> des fichiers traduits
est utilisé pour suivre la version des fichiers anglais sur laquelle elles
reposent. Si vous modifiez des fichiers traduits, il est nécessaire de mettre à
jour l'en-tête <q>translation-check</q> pour qu'il corresponde à l'empreinte de
la modification correspondante du fichier anglais. Vous pouvez trouver
cette empreinte avec la commande suivante :</p>

<pre>
$ git log chemin/vers/fichier/en_anglais/
</pre>

<p>
Si vous faites une nouvelle traduction de fichier, veuillez utiliser le
script <q>copypage.pl</q> et il créera un modèle pour votre langue, y compris
l'en-tête de traduction correct.</p>

<h4><a name="translation-smart-change">Modifications de traductions avec smart_change.pl</a></h4>

<p><code>smart_change.pl</code> est un script conçu pour faciliter la mise à
jour à la fois des fichiers originaux et de leurs traductions. Il y a deux
façons de l'utiliser, selon les modifications que vous faites.</p>

<p>Pour utiliser <code>smart_change</code> afin de mettre à jour seulement
les en-têtes « translation-check » quand vous travaillez manuellement sur les
fichiers :</p>

<ol>
  <li>modifiez le ou les fichiers originaux et envoyez les modifications ;</li>
  <li>mettez à jour les traductions ;</li>
  <li>exécutez smart_change.pl : le script récupérera les modifications et
    mettra à jour les en-têtes dans les fichiers de traduction ;</li>
  <li>vérifiez les modifications (par exemple avec « git diff ») ;</li>
  <li>envoyez les modifications de traduction.</li>
</ol>

<p>Sinon, si vous utilisez smart_change avec une expression rationnelle pour
faire plusieurs modifications dans plusieurs fichiers en une seule passe :</p>

<ol>
  <li>exécutez <code>smart_change.pl -s s/toto/titi/ fichier_origine1
    fichier_origine2 ...</code> ;</li>
  <li>vérifiez les modifications (par exemple avec « git diff ») ;</li>
  <li>envoyez les fichiers originaux ;</li>
  <li>exécutez <code>smart_change.pl fichier_origine1 fichier_origine2</code>
    (c'est-à-dire <strong>sans l'expression rationnelle</strong> cette
    fois-ci) ; cela mettra à jour uniquement les en-têtes dans les fichiers
    traduits ;</li>
  <li>finalement, envoyez les modifications de traduction.</li>
</ol>

<p>C'est plus compliqué qu'auparavant (deux envois sont nécessaires), mais
inévitable vue la manière dont fonctionnent les empreintes d'envoi de git.</p>

<h2><a name="notifications">Obtenir des notifications</a></h2>

<h3><a name="commit-notifications">Recevoir des notifications de modification</a></h3>
<p>
Nous avons configuré le projet webwml dans Salsa, de telle manière que les
modifications apparaissent sur le canal IRC #debian-www.</p>

<p>Si vous voulez recevoir des notifications par courriel lorsqu'il y a des
modifications dans le dépôt de webwml, veuillez vous abonner au pseudo-paquet
<q>www.debian.org</q> au moyen de tracker.debian.org et activez-y le mot clé
<q>vcs</q>, en suivant ces étapes (une seule fois) :</p>

<ul>
  <li>ouvrez un navigateur web et allez à l'adresse
      <url https://tracker.debian.org/pkg/www.debian.org> ;</li>
  <li>abonnez-vous au pseudo-paquet <q>www.debian.org</q> (vous pouvez vous
      authentifier par SSO ou enregistrer une adresse courriel et un mot de
      passe, si vous n'utilisez pas déjà tracker.debian.org avec un autre
      objectif) ;</li>
  <li>allez à la page <url https://tracker.debian.org/accounts/subscriptions/>,
      puis à <q>modify keywords</q>, cochez <q>vcs</q> (si ce n'est pas déjà
      fait) et sauvegardez ;</li>
  <li>à partir de ce moment, vous recevrez des courriels lorsque quelqu'un
      enverra une modification dans le dépôt webwml. D'autres dépôts de
      webmaster-team seront ajoutés prochainement.</li>
</ul>

<h3><a name="merge-request-notifications">Recevoir des notifications de Merge Request</a></h3>

<p>
Si vous voulez recevoir des notifications par courriel lorsqu'il y a de
nouvelles requêtes de fusion soumises sur l'interface web du dépôt webwml
de la plateforme GitLab de Salsa, vous pouvez configurer les réglages de
notification du dépôt webwml sur l'interface web, en suivant ces étapes :
</p>

<ul>
  <li>connectez-vous à votre compte Salsa et allez sur la page du projet ;</li>
  <li>cliquez sur l'icône cloche en haut de la page d'accueil du projet ;</li>
  <li>choisissez le niveau de notification que vous préférez.</li>
</ul>

#use wml::debian::translation-check translation="abccef21651668bce87fe0103d623399ffd369cd" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Guillaume Teissier a signalé que le client XMLRPC dans libxmlrpc3-java,
une implémentation XML-RPC dans Java, réalise la désérialisation de
l'exception côté serveur sérialisée dans l'attribut faultCause des messages
de réponse d'erreur de XMLRPC. Un serveur XMLRPC malveillant peut tirer
avantage de ce défaut pour exécuter du code arbitraire avec les privilèges
d'une application utilisant la bibliothèque client XMLRPC d'Apache.</p>

<p>Notez qu'un client qui s'attend à obtenir des exceptions côté serveur
doit configurer la propriété enabledForExceptions explicitement.</p>

<p>Pour la distribution oldstable (Stretch), ce problème a été corrigé dans
la version 3.1.3-8+deb9u1.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 3.1.3-9+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxmlrpc3-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxmlrpc3-java,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libxmlrpc3-java">\
https://security-tracker.debian.org/tracker/libxmlrpc3-java</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4619.data"
# $Id: $

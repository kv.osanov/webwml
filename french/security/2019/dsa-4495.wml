#use wml::debian::translation-check translation="2b74abc752f8e4232fe85da5b7c01782113a2f4d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20836">CVE-2018-20836</a>

<p>chenxiang a signalé une situation de compétition dans libsas, le
sous-système du noyau prenant en charge les périphériques SAS (Serial
Attached SCSI), qui pourrait conduire à une utilisation de mémoire après
libération. La manière dont cela pourrait être exploité n'est pas évidente.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1125">CVE-2019-1125</a>

<p>La plupart des processeurs x86 pourraient de façon spéculative ignorer
une instruction SWAPGS conditionnelle utilisée lorsqu'elle est entrée dans
le noyau à partir de l'espace utilisateur, et/ou pourraient l'exécuter de
façon spéculative alors qu'elle devrait être ignorée. Il s'agit d'un
sous-type de Spectre variante 1 qui pourrait permettre à des utilisateurs
locaux d'obtenir des informations sensibles du noyau ou d'autres processus.
Le problème a été pallié en utilisant des barrières de mémoire pour limiter
l'exécution spéculative. Les systèmes utilisant un noyau i386 ne sont pas
affectés dans le mesure où le noyau n'utilise pas d'instruction SWAPGS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1999">CVE-2019-1999</a>

<p>Une situation de compétition a été découverte dans un pilote de création
de lien d'Android qui pourrait conduire à une utilisation de mémoire après
libération. Si ce pilote est chargé, un utilisateur local pourrait être
capable d'utiliser cela pour un déni de service (corruption de mémoire) ou
pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10207">CVE-2019-10207</a>

<p>L'outil syzkaller a découvert un éventuel déréférencement de pointeur
NULL dans divers pilotes pour des adaptateurs Bluetooth avec une connexion
UART. Un utilisateur local doté d'un accès à un périphérique de
pseudo-terminal ou un autre périphérique tty adapté pourrait utiliser cela
pour un déni de service (bogue ou oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10638">CVE-2019-10638</a>

<p>Amit Klein et Benny Pinkas ont découvert que la génération
d'identifiants de paquet d'IP utilisait une fonction de hachage faible,
<q>jhash</q>. Cela pourrait permettre de pister des ordinateurs
particuliers alors qu'ils communiquent avec divers serveurs distants et
à partir de réseaux différents. La fonction <q>siphash</q> est maintenant
utilisée à la place.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12817">CVE-2019-12817</a>

<p>Sur l'architecture PowerPC (ppc64el), le code de table de hachage des
pages (HPT) ne gérait pas correctement hfork() dans un processus mappé en
mémoire à des adresses supérieures à 512 To. Cela pourrait conduire à une
utilisation de mémoire après libération dans le noyau, ou au partage
involontaire de mémoire entre des processus de l'utilisateur. Un
utilisateur local pourrait utiliser cela pour une élévation des privilèges.
Les machines utilisant l'unité de gestion de mémoire de base ou un noyau
personnalisé avec une taille de page de 4 ko ne sont pas affectées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12984">CVE-2019-12984</a>

<p>L'implémentation du protocole NFC ne validait pas correctement un
message de contrôle de netlink, menant éventuellement à un déréférencement
de pointeur NULL. Un utilisateur local sur une machine avec une interface
NFC pourrait utiliser cela pour un déni de service (bogue ou oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13233">CVE-2019-13233</a>

<p>Jann Horn a découvert une situation de compétition sur l'architecture
x86 dans l'utilisation de la table locale de descripteurs (LDT). Cela
pourrait conduire à une utilisation de mémoire après libération. Un
utilisateur local pourrait éventuellement utiliser cela pour un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13631">CVE-2019-13631</a>

<p>Le pilote gtco pour les tablettes graphiques USB pourrait provoquer un
débordement de tampon de pile avec des données constantes lors de
l'analyse du descripteur du périphérique. Un utilisateur physiquement
présent avec un périphérique USB spécialement construit pourrait utiliser
cela pour provoquer un déni de service (bogue ou oops), ou éventuellement
pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13648">CVE-2019-13648</a>

<p>Praveen Pandey a signalé que sur les systèmes PowerPC (ppc64el) sans
mémoire transactionnelle (TM), le noyau pourrait néanmoins tenter de
restaurer l'état de TM passé à l'appel système sigreturn(). Un utilisateur
local pourrait utiliser cela pour un déni de service (oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14283">CVE-2019-14283</a>

<p>L'outil syzkaller a découvert une absence de vérification de limites
dans le pilote de disquette. Un utilisateur local doté d'un accès à un
lecteur de disquette, avec une disquette présente, pourrait utiliser cela
pour lire la mémoire du noyau au-delà du tampon d'entrée/sortie, obtenant
éventuellement des informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14284">CVE-2019-14284</a>

<p>L'outil syzkaller a découvert une éventuelle division par zéro dans le
pilote de disquette. Un utilisateur local doté d'un accès à un lecteur de
disquette pourrait utiliser cela pour un déni de service (oops).</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 4.19.37-5+deb10u2.</p>

<p>Pour la distribution oldstable (Stretch), ces problèmes seront corrigés
prochainement.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4495.data"
# $Id: $

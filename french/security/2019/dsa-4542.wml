#use wml::debian::translation-check translation="6f7800b4562d66c25875afecfd9f8039b52eaf60" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>jackson-databind, une bibliothèque Java utilisée pour analyser des
données aux formats JSON et autres, ne validait pas correctement les
entrées de l'utilisateur avant de tenter la désérialisation. Cela
permettait à un attaquant de fournir des entrées malveillantes pour
réaliser une exécution de code ou lire des fichiers arbitraires sur le
serveur.</p>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 2.8.6-1+deb9u6.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2.9.8-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jackson-databind.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jackson-databind,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/jackson-databind">\
https://security-tracker.debian.org/tracker/jackson-databind</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4542.data"
# $Id: $
